package ec.com.redepronik.negosys.seguridad.aspect;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Id;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import ec.com.redepronik.negosys.rrhh.service.PersonaService;
import ec.com.redepronik.negosys.seguridad.dao.BitacoraDao;
import ec.com.redepronik.negosys.seguridad.entity.Bitacora;

@Component
@Aspect
public class BitacoraAspect implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	public BitacoraDao bitacoraDao;

	@Autowired
	public PersonaService personaService;

	@After("execution(public * ec.com.redepronik.negosys.invfac.service..*.eliminar(..)) "
			+ "|| execution(public * ec.com.redepronik.negosys.rrhh.service..*.eliminar(..)) ")
	public void auditarEliminarNegosys(JoinPoint joinPoint) {
		String cedula = SecurityContextHolder.getContext().getAuthentication().getName();
		if (cedula.compareTo("0123456789") != 0) {
			Object obj = (joinPoint.getArgs())[0];
			String mensaje = "";
			try {
				String nombre = nombreObjetoId(obj);
				Method metodo = obj.getClass().getMethod(nombre, new Class[] {});
				Method metodoEliminar = obj.getClass().getMethod("getActivo", new Class[] {});

				if ((Boolean) metodoEliminar.invoke(obj, new Object[] {}))
					mensaje = "Se Activó el objeto " + obj.getClass().getSimpleName() + " con la llave: # "
							+ metodo.invoke(obj, new Object[] {});
				else
					mensaje = "Se Desactivó el objeto " + obj.getClass().getSimpleName() + " con la llave: # "
							+ metodo.invoke(obj, new Object[] {});

			} catch (Exception e) {
				e.printStackTrace();
				mensaje = "error al leer método";
			}
			bitacoraDao.insertar(new Bitacora(new Timestamp((new Date()).getTime()), mensaje,
					personaService.obtenerPorCedula(cedula)));
		}
	}

	@After("execution(public * ec.com.redepronik.negosys.invfac.service..*.insertar(..)) "
			+ "|| execution(public * ec.com.redepronik.negosys.invfac.service..*.actualizar(..)) "
			+ "|| execution(public * ec.com.redepronik.negosys.rrhh.service..*.insertar(..)) "
			+ "|| execution(public * ec.com.redepronik.negosys.rrhh.service..*.actualizar(..)) ")
	public void auditarNegosys(JoinPoint joinPoint) {
		String cedula = SecurityContextHolder.getContext().getAuthentication().getName();
		if (cedula.compareTo("0123456789") != 0) {
			Object obj = (joinPoint.getArgs())[0];
			String mensaje = "";
			try {
				String nombre = nombreObjetoId(obj);
				Method metodo = obj.getClass().getMethod(nombre, new Class[] {});
				mensaje = "Se acaba de " + joinPoint.getSignature().getName() + " el objeto "
						+ obj.getClass().getSimpleName() + " con la llave: # " + metodo.invoke(obj, new Object[] {});
			} catch (Exception e) {
				e.printStackTrace();
				mensaje = "error al leer método";
			}

			bitacoraDao.insertar(new Bitacora(new Timestamp((new Date()).getTime()), mensaje,
					personaService.obtenerPorCedula(cedula)));
		}
	}

	@After("execution(public * ec.com.redepronik.negosys.seguridad.service.MenuService.obtenerPorUsuario(..)) ")
	public void ingresoNegosys(JoinPoint joinPoint) {
		String cedula = SecurityContextHolder.getContext().getAuthentication().getName();
		if (cedula.compareTo("0123456789") != 0)
			bitacoraDao.insertar(new Bitacora(new Timestamp((new Date()).getTime()), "Ingresó al Sistema",
					personaService.obtenerActivoPorCedula(cedula)));
	}

	private String nombreObjetoId(Object obj) {
		Class<?> c = obj.getClass();
		for (Field field : c.getDeclaredFields())
			if (field.isAnnotationPresent(Id.class) || field.isAnnotationPresent(EmbeddedId.class)) {
				field.setAccessible(true);
				return field.getName();
			}

		for (Method method : c.getDeclaredMethods())
			if (method.isAnnotationPresent(Id.class) || method.isAnnotationPresent(EmbeddedId.class))
				return method.getName();
		return null;
	}

}