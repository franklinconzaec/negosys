package ec.com.redepronik.negosys.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private Environment env;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf()
				.disable()
				.authorizeRequests()
				.antMatchers("/javax.faces.resource/**", "/resources/**",
						"/login.jsf").permitAll()
				.antMatchers("/views/home.jsf")
				.access("isAuthenticated()")

				.antMatchers("/views/directorio/cliente.jsf")
				.hasAnyAuthority("ADMI", "CAJA", "VEND")
				.antMatchers("/views/directorio/proveedor.jsf")
				.hasAnyAuthority("ADMI", "BODE")
				.antMatchers("/views/directorio/reportes.jsf")
				.hasAnyAuthority("ADMI", "BODE", "CAJA", "VEND")

				.antMatchers("/views/inventario/producto.jsf")
				.hasAnyAuthority("ADMI", "BODE")
				.antMatchers("/views/inventario/listadoIngreso.jsf")
				.hasAnyAuthority("ADMI", "BODE")
				.antMatchers("/views/inventario/ingreso.jsf")
				.hasAnyAuthority("ADMI", "BODE")
				.antMatchers("/views/inventario/kardex.jsf")
				.hasAnyAuthority("ADMI", "CAJA", "BODE", "VEND")
				.antMatchers("/views/inventario/listadoBajaInventario.jsf")
				.hasAnyAuthority("ADMI", "BODE")
				.antMatchers("/views/inventario/bajaInventario.jsf")
				.hasAnyAuthority("ADMI", "BODE")
				.antMatchers("/views/inventario/traspasoBodega.jsf")
				.hasAnyAuthority("ADMI", "BODE")
				.antMatchers("/views/inventario/reportes.jsf")
				.hasAnyAuthority("ADMI", "BODE")

				.antMatchers("/views/facturacion/listadoFactura.jsf")
				.hasAnyAuthority("ADMI", "CAJA")
				.antMatchers("/views/facturacion/factura.jsf")
				.hasAnyAuthority("ADMI", "CAJA")
				.antMatchers("/views/facturacion/facturaCajero.jsf")
				.hasAnyAuthority("ADMI", "CAJA")
				.antMatchers("/views/facturacion/facturaInterna.jsf")
				.hasAnyAuthority("ADMI")
				.antMatchers("/views/facturacion/listadoCotizacion.jsf")
				.hasAnyAuthority("ADMI", "CAJA", "VEND")
				.antMatchers("/views/facturacion/cotizacion.jsf")
				.hasAnyAuthority("ADMI", "CAJA", "VEND")
				.antMatchers("/views/facturacion/listadoPedido.jsf")
				.hasAnyAuthority("ADMI", "CAJA", "VEND")
				.antMatchers("/views/facturacion/pedido.jsf")
				.hasAnyAuthority("ADMI", "CAJA", "VEND")
				.antMatchers("/views/facturacion/listadoNotaCredito.jsf")
				.hasAnyAuthority("ADMI", "CAJA")
				.antMatchers("/views/facturacion/notaCredito.jsf")
				.hasAnyAuthority("ADMI", "CAJA")
				.antMatchers("/views/facturacion/listadoGuiaRemision.jsf")
				.hasAnyAuthority("ADMI", "CAJA")
				.antMatchers("/views/facturacion/guiaRemision.jsf")
				.hasAnyAuthority("ADMI", "CAJA")
				.antMatchers("/views/facturacion/reportes.jsf")
				.hasAnyAuthority("ADMI", "CAJA")

				.antMatchers("/views/cuentasPorPagar/pago.jsf")
				.hasAnyAuthority("ADMI", "CAJA")
				.antMatchers("/views/cuentasPorPagar/gastos.jsf")
				.hasAnyAuthority("ADMI", "CAJA")
				.antMatchers("/views/cuentasPorPagar/reportes.jsf")
				.hasAnyAuthority("ADMI", "CAJA")

				.antMatchers("/views/cuentasPorCobrar/cobro.jsf")
				.hasAnyAuthority("ADMI", "CAJA")
				.antMatchers("/views/cuentasPorCobrar/cobroCredito.jsf")
				.hasAnyAuthority("ADMI", "CAJA")
				.antMatchers("/views/cuentasPorCobrar/credito.jsf")
				.hasAnyAuthority("ADMI", "CAJA")
				.antMatchers("/views/cuentasPorCobrar/reportes.jsf")
				.hasAnyAuthority("ADMI", "CAJA")
				
				
				.antMatchers("/views/tributacion/listadoRetencionesCompra.jsf")
				.hasAnyAuthority("ADMI", "BODE")
				.antMatchers("/views/tributacion/retencionCompra.jsf")
				.hasAnyAuthority("ADMI", "BODE")
				.antMatchers("/views/tributacion/listadoRetencionesVenta.jsf")
				.hasAnyAuthority("ADMI", "BODE")
				.antMatchers("/views/tributacion/listadoAnulados.jsf")
				.hasAnyAuthority("ADMI", "BODE")
				.antMatchers("/views/tributacion/reportes.jsf")
				.hasAnyAuthority("ADMI", "BODE")
				
				.antMatchers("/views/procesos/firmarComprobantes.jsf")
				.hasAnyAuthority("ADMI")
				.antMatchers("/views/procesos/envioComprobantes.jsf")
				.hasAnyAuthority("ADMI")
				.antMatchers("/views/procesos/subirComprobantes.jsf")
				.hasAnyAuthority("ADMI")
				.antMatchers("/views/procesos/visualizacionRide.jsf")
				.hasAnyAuthority("ADMI")
				.antMatchers("/views/procesos/reEnvioComprobantes.jsf")
				.hasAnyAuthority("ADMI")
				
				.antMatchers("/views/parametros/configuracion.jsf")
				.hasAnyAuthority("ADMI")
				.antMatchers("/views/parametros/bitacora.jsf")
				.hasAnyAuthority("ADMI")

				.antMatchers("/views/rrhh/cargo.jsf")
				.hasAnyAuthority("ADMI")
				.antMatchers("/views/rrhh/empleado.jsf")
				.hasAnyAuthority("ADMI")
				
				.antMatchers("/views/m/cliente.jsf")
				.hasAnyAuthority("ADMI")
				.antMatchers("/views/m/listadoPedido.jsf")
				.hasAnyAuthority("ADMI")
				.antMatchers("/views/m/pedido.jsf")
				.hasAnyAuthority("ADMI")
				
				.antMatchers("/views/seguridad/404.jsf")
				.access("isAuthenticated()")
				.antMatchers("/views/seguridad/accesoDenegado.jsf")
				.access("isAuthenticated()")
				.antMatchers("/views/seguridad/cambiarClave.jsf")
				.access("isAuthenticated()")
				.antMatchers("/views/seguridad/cambiarClaveNueva.jsf")
				.access("isAuthenticated()")
				.antMatchers("/views/seguridad/error.jsf")
				.access("isAuthenticated()")
				.antMatchers("/views/seguridad/errorPago.jsf")
				.access("isAuthenticated()")
								
				.and().formLogin().loginPage("/login.jsf")
				.defaultSuccessUrl("/views/home.jsf")
				.and().logout().logoutUrl("/logout.jsf")
				.logoutSuccessUrl("/login.jsf")
				.invalidateHttpSession(true)
				.deleteCookies("JSESSIONID")
				.and().sessionManagement()
				.invalidSessionUrl("/login.jsf")
				.maximumSessions(1);
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth)
			throws Exception {
		PersistenceConfig persistenceConfig = new PersistenceConfig();

		auth.jdbcAuthentication().dataSource(persistenceConfig.dataSource())
				.passwordEncoder(new ShaPasswordEncoder(256))
				.usersByUsernameQuery(getUserQuery())
				.authoritiesByUsernameQuery(getAuthoritiesQuery());
	}

	private String getAuthoritiesQuery() {
		return "select p.cedula , r.nombre "
				+ "from negosys.persona as p, negosys.rol as r, negosys.rolusuario as ur "
				+ "where p.personaid = ur.personaid and r.rolid = ur.rolid and ur.activo=true and p.cedula = ?";
	}

	private String getUserQuery() {
		return "select cedula, password, activo from negosys.persona "
				+ "where cedula = ?";
	}
}
