package ec.com.redepronik.negosys.invfac.dao;

import org.springframework.stereotype.Repository;

import ec.com.redepronik.negosys.invfac.entity.Anexos;
import ec.com.redepronik.negosys.utils.dao.GenericDaoImpl;

@Repository
public class AnexosDaoImpl extends GenericDaoImpl<Anexos, Integer> implements AnexosDao {

}