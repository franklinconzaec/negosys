package ec.com.redepronik.negosys.invfac.dao;

import ec.com.redepronik.negosys.invfac.entityAux.EntregaProductoReporte;
import ec.com.redepronik.negosys.utils.dao.GenericDao;

public interface EntregaProductoReporteDao extends GenericDao<EntregaProductoReporte, Integer> {

}