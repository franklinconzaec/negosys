package ec.com.redepronik.negosys.invfac.entityAux;

import java.io.Serializable;
import java.util.List;

import ec.com.redepronik.negosys.invfac.entity.Retencion;

public class ListRetencionImprimir implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Retencion> listRetenciones;

	public ListRetencionImprimir() {

	}

	public ListRetencionImprimir(List<Retencion> listRetenciones) {
		this.listRetenciones = listRetenciones;
	}

	public List<Retencion> getListRetenciones() {
		return listRetenciones;
	}

	public void setListRetenciones(List<Retencion> listRetenciones) {
		this.listRetenciones = listRetenciones;
	}

}
