package ec.com.redepronik.negosys.invfac.service;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import ec.com.redepronik.negosys.invfac.entity.Anulado;
import ec.com.redepronik.negosys.invfac.entity.TipoComprobante;

public interface AnuladoService {

	@Transactional
	public boolean insertar(Anulado anulado);

	@Transactional
	public Anulado obtenerPorId(Integer id);

	@Transactional
	public List<Anulado> obtener(TipoComprobante criterioBusquedaTipoComprobante,
			String criterioBusquedaNumeroComprobante, Date criterioBusquedaFecha);

	@Transactional
	public List<Anulado> obtenerParaAnexo(Date fechaInicio, Date fechaFin);
}