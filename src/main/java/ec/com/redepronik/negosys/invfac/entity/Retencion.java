package ec.com.redepronik.negosys.invfac.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import ec.com.redepronik.negosys.rrhh.entity.EmpleadoCargo;
import ec.com.redepronik.negosys.rrhh.entity.Proveedor;

@Entity
@Table(name = "retenciones")
public class Retencion implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private Proveedor proveedor;
	private EmpleadoCargo cajero;
	private Timestamp fechaEmision;
	private String establecimiento;
	private String puntoEmision;
	private String secuencia;
	private Boolean activo;
	private String autorizacion;
	private SustentoTributario sustentoTributario;
	private TipoComprobante tipoCompraRetencion;
	private String numeroCompra;
	private String autorizacionCompra;
	private Date fechaCompra;
	private BigDecimal base0;
	private BigDecimal base12;
	private List<DetalleRetencion> detallesRetenciones;

	public Retencion() {
	}

	public Retencion(Integer id, Proveedor proveedor, EmpleadoCargo cajero, Timestamp fechaEmision,
			String establecimiento, String puntoEmision, String secuencia, Boolean activo, String autorizacion,
			SustentoTributario sustentoTributario, TipoComprobante tipoCompraRetencion, String numeroCompra,
			String autorizacionCompra, Date fechaCompra, BigDecimal base0, BigDecimal base12,
			List<DetalleRetencion> detallesRetenciones) {
		this.id = id;
		this.proveedor = proveedor;
		this.cajero = cajero;
		this.fechaEmision = fechaEmision;
		this.establecimiento = establecimiento;
		this.puntoEmision = puntoEmision;
		this.secuencia = secuencia;
		this.activo = activo;
		this.autorizacion = autorizacion;
		this.sustentoTributario = sustentoTributario;
		this.tipoCompraRetencion = tipoCompraRetencion;
		this.numeroCompra = numeroCompra;
		this.autorizacionCompra = autorizacionCompra;
		this.fechaCompra = fechaCompra;
		this.base0 = base0;
		this.base12 = base12;
		this.detallesRetenciones = detallesRetenciones;
	}

	public DetalleRetencion addDetalleRetencion(DetalleRetencion detalleRetencion) {
		getDetallesRetenciones().add(detalleRetencion);
		detalleRetencion.setRetencion(this);

		return detalleRetencion;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Retencion other = (Retencion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Column(nullable = false)
	public Boolean getActivo() {
		return this.activo;
	}

	@ManyToOne
	@JoinColumn(name = "cajero", nullable = false)
	public EmpleadoCargo getCajero() {
		return cajero;
	}

	@OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "retencion")
	// @IndexColumn(name = "orden", base = 1)
	public List<DetalleRetencion> getDetallesRetenciones() {
		return detallesRetenciones;
	}

	@Column(length = 3, nullable = false)
	public String getEstablecimiento() {
		return establecimiento;
	}

	@Column(name = "fechaemision", nullable = false)
	public Timestamp getFechaEmision() {
		return fechaEmision;
	}

	@Id
	@SequenceGenerator(allocationSize = 1, name = "RETENCIONES_ID_GENERATOR", sequenceName = "RETENCIONES_ID_SEQ")
	// @GeneratedValue(strategy = GenerationType.IDENTITY)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RETENCIONES_ID_GENERATOR")
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	@ManyToOne
	@JoinColumn(name = "proveedor")
	public Proveedor getProveedor() {
		return proveedor;
	}

	@Column(name = "puntoemision", length = 3, nullable = false)
	public String getPuntoEmision() {
		return puntoEmision;
	}

	@Column(length = 9, nullable = false)
	public String getSecuencia() {
		return secuencia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Column(name = "fechacompra", nullable = false)
	public Date getFechaCompra() {
		return fechaCompra;
	}

	@Column(name = "numerocompra", length = 15, nullable = false)
	public String getNumeroCompra() {
		return numeroCompra;
	}

	@Column(name = "autorizacioncompra", length = 37, nullable = false)
	public String getAutorizacionCompra() {
		return autorizacionCompra;
	}

	@Column(name = "autorizacion", length = 37)
	public String getAutorizacion() {
		return autorizacion;
	}

	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "tipocompra", nullable = false)
	public TipoComprobante getTipoCompraRetencion() {
		return tipoCompraRetencion;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "sustentotributario", nullable = false)
	public SustentoTributario getSustentoTributario() {
		return sustentoTributario;
	}

	@Column(name = "base0", precision = 8, scale = 2, nullable = false)
	public BigDecimal getBase0() {
		return base0;
	}

	public void setBase0(BigDecimal base0) {
		this.base0 = base0;
	}

	@Column(name = "base12", precision = 8, scale = 2, nullable = false)
	public BigDecimal getBase12() {
		return base12;
	}

	public void setBase12(BigDecimal base12) {
		this.base12 = base12;
	}

	public void setAutorizacionCompra(String autorizacionCompra) {
		this.autorizacionCompra = autorizacionCompra;
	}

	public void setFechaCompra(Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public void setNumeroCompra(String numeroCompra) {
		this.numeroCompra = numeroCompra;
	}

	public void setTipoCompraRetencion(TipoComprobante tipoCompraRetencion) {
		this.tipoCompraRetencion = tipoCompraRetencion;
	}

	public void setSustentoTributario(SustentoTributario sustentoTributario) {
		this.sustentoTributario = sustentoTributario;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public DetalleRetencion removeDetalleRetencion(DetalleRetencion detalleRetencion) {
		getDetallesRetenciones().remove(detalleRetencion);
		detalleRetencion.setRetencion(null);

		return detalleRetencion;
	}

	public void setCajero(EmpleadoCargo cajero) {
		this.cajero = cajero;
	}

	public void setDetallesRetenciones(List<DetalleRetencion> detallesRetenciones) {
		this.detallesRetenciones = detallesRetenciones;
	}

	public void setEstablecimiento(String establecimiento) {
		this.establecimiento = establecimiento;
	}

	public void setFechaEmision(Timestamp fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public void setPuntoEmision(String puntoEmision) {
		this.puntoEmision = puntoEmision;
	}

	public void setSecuencia(String secuencia) {
		this.secuencia = secuencia;
	}
}