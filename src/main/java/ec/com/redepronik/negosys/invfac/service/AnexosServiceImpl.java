package ec.com.redepronik.negosys.invfac.service;

import static ec.com.redepronik.negosys.utils.UtilsAplicacion.presentaMensaje;
import static ec.com.redepronik.negosys.utils.UtilsArchivos.convertirString;
import static ec.com.redepronik.negosys.utils.UtilsDate.timestamp;
import static ec.com.redepronik.negosys.utils.UtilsMath.newBigDecimal;
import static ec.com.redepronik.negosys.utils.UtilsMath.parametro;
import static ec.com.redepronik.negosys.utils.UtilsMath.redondearTotales;
import static ec.com.redepronik.negosys.utils.UtilsXML.objetoTOxml;
import static ec.com.redepronik.negosys.utils.UtilsXML.xmlTOobjeto;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.com.redepronik.negosys.invfac.dao.AnexosDao;
import ec.com.redepronik.negosys.invfac.entity.Anexos;
import ec.com.redepronik.negosys.invfac.entityAux.Anio;
import ec.com.redepronik.negosys.invfac.entityAux.Mes;
import ec.com.redepronik.negosys.utils.anexo.Anexo;
import ec.com.redepronik.negosys.utils.anexo.Anulados;
import ec.com.redepronik.negosys.utils.anexo.Compras;
import ec.com.redepronik.negosys.utils.anexo.DetalleCompras;
import ec.com.redepronik.negosys.utils.anexo.Ventas;
import ec.com.redepronik.negosys.utils.anexo.Ventas.DetalleVentas;
import ec.com.redepronik.negosys.utils.anexo.VentasEstablecimiento;

@Service
public class AnexosServiceImpl implements AnexosService {

	@Autowired
	private AnexosDao anexosDao;

	public List<Anexos> obtenerPorMes(Anio anio, Mes mes) {
		List<Anexos> list = null;
		if (anio == null || mes == null)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "INGRESE UN CRITERIO DE BUSQUEDA");
		else {
			list = anexosDao.obtenerPorHql("select a from Anexos a " + "where a.anio=?1 and a.mes=?2 order by a.fecha",
					new Object[] { anio.getId(), mes.getId() });

			if (list == null || list.isEmpty())
				presentaMensaje(FacesMessage.SEVERITY_INFO, "No se encontraron coincidencias");
		}
		return list;
	}

	public void insertar(InputStream xlsx) {
		Anexo a = (Anexo) xmlTOobjeto(Anexo.class, xlsx);
		if (a == null) {
			presentaMensaje(FacesMessage.SEVERITY_INFO, "El archivo no es valido");
		} else {
			if (parametro.getRuc().compareToIgnoreCase(a.getIdInformante()) == 0) {
				Anexos anexos = new Anexos();
				anexos.setAnio(a.getAnio());
				anexos.setMes(a.getMes());
				anexos.setFecha(timestamp());
				anexos.setAnexo(objetoTOxml(Anexo.class, a));
				BigDecimal tventa0 = newBigDecimal();
				BigDecimal tventa12 = newBigDecimal();
				BigDecimal tcompra0 = newBigDecimal();
				BigDecimal tcompra12 = newBigDecimal();
				if (a.getVentas() != null)
					for (DetalleVentas dv : a.getVentas().getDetalleVentas()) {
						tventa0 = tventa0.add(dv.getBaseImponible());
						tventa12 = tventa12.add(dv.getBaseImpGrav());
					}
				if (a.getCompras() != null)
					for (DetalleCompras dc : a.getCompras().getDetalleCompras()) {
						tcompra0 = tcompra0.add(dc.getBaseImponible());
						tcompra12 = tcompra12.add(dc.getBaseImpGrav());
					}
				anexos.setTventa0(redondearTotales(tventa0));
				anexos.setTventa12(redondearTotales(tventa12));
				anexos.setTcompra0(redondearTotales(tcompra0));
				anexos.setTcompra12(redondearTotales(tcompra12));

				anexosDao.insertar(anexos);
				presentaMensaje(FacesMessage.SEVERITY_INFO, "Se Inserto Correctamente");
			} else
				presentaMensaje(FacesMessage.SEVERITY_INFO, "El Anexo no corresponde al RUC configurado en el Sistema");
		}
	}

	public void eliminar(Anexos anexos) {
		anexosDao.eliminar(anexos);
		presentaMensaje(FacesMessage.SEVERITY_INFO, "Se Elimino Correctamente");
	}

	public Anexo generarAnexoFinal(Anio anio, Mes mes) {
		List<Anexo> list = new ArrayList<Anexo>();
		for (Anexos a : obtenerPorMes(anio, mes)) {
			list.add((Anexo) xmlTOobjeto(Anexo.class, new ByteArrayInputStream(convertirString(a.getAnexo()))));
		}
		Anexo anexoFinal = null;
		if (list.size() == 1) {
			anexoFinal = list.get(0);
		} else if (list.size() > 1) {
			anexoFinal = list.get(0);
			Anexo anexoAux = list.get(1);
			anexoFinal.setNumEstabRuc(String.format("%03d", 2));
			anexoFinal.setTotalVentas(anexoFinal.getTotalVentas().add(anexoAux.getTotalVentas()));
			if (anexoFinal.getCompras() == null)
				anexoFinal.setCompras(new Compras());
			if (anexoAux.getCompras() != null)
				anexoFinal.getCompras().getDetalleCompras().addAll(anexoAux.getCompras().getDetalleCompras());
			if (anexoFinal.getVentas() == null)
				anexoFinal.setVentas(new Ventas());
			if (anexoAux.getVentas() != null) {
				List<DetalleVentas> listDv = anexoFinal.getVentas().getDetalleVentas();
				List<DetalleVentas> listDvAux = anexoAux.getVentas().getDetalleVentas();
				for (int i = 0; i < listDv.size(); i++) {
					DetalleVentas dv = listDv.get(i);
					for (int j = 0; j < listDvAux.size(); j++) {
						DetalleVentas dvAux = listDvAux.get(j);
						if (dv.getIdCliente().compareToIgnoreCase(dvAux.getIdCliente()) == 0
								&& dv.getTipoComprobante().compareToIgnoreCase(dvAux.getTipoComprobante()) == 0) {
							dv.setNumeroComprobantes(dv.getNumeroComprobantes() + dvAux.getNumeroComprobantes());
							dv.setBaseNoGraIva(dv.getBaseNoGraIva().add(dvAux.getBaseNoGraIva()));
							dv.setBaseImponible(dv.getBaseImponible().add(dvAux.getBaseImponible()));
							dv.setBaseImpGrav(dv.getBaseImpGrav().add(dvAux.getBaseImpGrav()));
							dv.setMontoIva(dv.getMontoIva().add(dvAux.getMontoIva()));
							dv.setValorRetIva(dv.getValorRetIva().add(dvAux.getValorRetIva()));
							dv.setValorRetRenta(dv.getValorRetRenta().add(dvAux.getValorRetRenta()));
							listDvAux.remove(dvAux);
						}
					}
				}
				listDv.addAll(listDvAux);
				anexoFinal.setVentas(new Ventas());
				anexoFinal.getVentas().getDetalleVentas().addAll(listDv);
			}

			if (anexoFinal.getVentasEstablecimiento() == null)
				anexoFinal.setVentasEstablecimiento(new VentasEstablecimiento());
			if (anexoAux.getVentasEstablecimiento() != null)
				anexoFinal.getVentasEstablecimiento().getDetalleVentasEst()
						.addAll(anexoAux.getVentasEstablecimiento().getDetalleVentasEst());
			if (anexoFinal.getAnulados() == null)
				anexoFinal.setAnulados(new Anulados());
			if (anexoAux.getAnulados() != null)
				anexoFinal.getAnulados().getDetalleAnulados().addAll(anexoAux.getAnulados().getDetalleAnulados());
		}
		return anexoFinal;
	}
}
