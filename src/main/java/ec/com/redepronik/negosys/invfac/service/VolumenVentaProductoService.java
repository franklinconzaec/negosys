package ec.com.redepronik.negosys.invfac.service;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import ec.com.redepronik.negosys.invfac.entity.EstadoProductoVenta;
import ec.com.redepronik.negosys.invfac.entityAux.VolumenVentaProducto;

public interface VolumenVentaProductoService {

	@Transactional
	public List<VolumenVentaProducto> obtenerVolumenVentaProducto(Date fechaInicio, Date fechaFin, int ordenarId,
			EstadoProductoVenta estadoProductoVenta);

	@Transactional
	public List<VolumenVentaProducto> obtenerVolumenVentaProductoPorVendedor(Date fechaInicio, Date fechaFin,
			int proveedorId, int vendedorId);
}
