package ec.com.redepronik.negosys.invfac.service;

import java.io.InputStream;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import ec.com.redepronik.negosys.invfac.entity.Anexos;
import ec.com.redepronik.negosys.invfac.entityAux.Anio;
import ec.com.redepronik.negosys.invfac.entityAux.Mes;
import ec.com.redepronik.negosys.utils.anexo.Anexo;

public interface AnexosService {

	@Transactional
	public List<Anexos> obtenerPorMes(Anio anio, Mes mes);

	@Transactional
	public void insertar(InputStream xlsx);

	@Transactional
	public void eliminar(Anexos anexos);

	@Transactional
	public Anexo generarAnexoFinal(Anio anio, Mes mes);

}