package ec.com.redepronik.negosys.invfac.dao;

import ec.com.redepronik.negosys.invfac.entityAux.CantidadBajaInventarioReporte;
import ec.com.redepronik.negosys.utils.dao.GenericDao;

public interface CantidadBajaInventarioReporteDao extends GenericDao<CantidadBajaInventarioReporte, Integer> {

}