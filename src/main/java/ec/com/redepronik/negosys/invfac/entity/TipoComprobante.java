package ec.com.redepronik.negosys.invfac.entity;

public enum TipoComprobante {

	C01("01", "FACTURA"), C02("02", "NOTA O BOLETA DE VENTA"), C03("03",
			"LIQUIDACIÓN DE COMPRA DE BIENES O PRESTACIÓN DE SERVICIOS"), C04("04", "NOTA DE CRÉDITO"), C05("05",
					"NOTA DE DÉBITO"), C06("06", "GUIAS DE REMISION"), C07("07", "COMPROBANTE DE RETENCIONES"), C11(
							"11", "PASAJES EXPEDIDOS POR EMPRESAS DE AVIACIÓN"), C12("12",
									"DOCUMENTOS EMITIDO POR INSTITUCIONES FINANCIERAS"), C15("15",
											"COMPROBANTE DE VENTA EMITIDO EN EL EXTERIOR"), C19("19",
													"COMPROBANTES DE PAGO DE CUOTAS O APORTES"), C20("20",
															"DOCUMENTOS POR SERVICIOS ADM. EMITIDOS POR INST. DEL ESTADO"), C21(
																	"21", "CARTA DE PORTE AÉREO"), C41("41",
																			"COMPROBANTE DE VENTA EMITIDO POR REEMBOLSO"), C42(
																					"42",
																					"DOCUMENTO AGENTE RETENCIÓN PRESUNTIVA"), C43(
																							"43",
																							"LIQUIDACIÓN PARA EXPLOTACION Y EXPLORACION DE HIDROCARBUROS"), C45(
																									"45",
																									"LIQUIDACIÓN DE MEDICINA PREPAGADA"), C47(
																											"47",
																											"N/C POR REEMBOLSO EMITIDA POR INTERMEDIARIO"), C48(
																													"48",
																													"N/D POR REEMBOLSO EMITIDA POR INTERMEDIARIO");

	private final String id;
	private final String nombre;

	private TipoComprobante(String id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}

	public String getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public static TipoComprobante obtenerPorId(String id) {
		for (TipoComprobante tc : TipoComprobante.values())
			if (tc.id.compareToIgnoreCase(id) == 0)
				return tc;
		return null;
	}
}
