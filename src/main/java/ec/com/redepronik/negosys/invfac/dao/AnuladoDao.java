package ec.com.redepronik.negosys.invfac.dao;

import ec.com.redepronik.negosys.invfac.entity.Anulado;
import ec.com.redepronik.negosys.utils.dao.GenericDao;

public interface AnuladoDao extends GenericDao<Anulado, Integer> {

}