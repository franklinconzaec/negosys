package ec.com.redepronik.negosys.invfac.controller;

import static ec.com.redepronik.negosys.utils.UtilsAplicacion.presentaMensaje;
import static ec.com.redepronik.negosys.utils.UtilsMath.newBigDecimal;
import static ec.com.redepronik.negosys.utils.UtilsMath.redondearTotales;
import static ec.com.redepronik.negosys.utils.documentosElectronicos.UtilsDocumentos.tipoIdentificacionComprador;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import ec.com.redepronik.negosys.invfac.entity.DetalleFactura;
import ec.com.redepronik.negosys.invfac.entity.Factura;
import ec.com.redepronik.negosys.invfac.entityAux.CantidadFactura;
import ec.com.redepronik.negosys.invfac.entityAux.FacturaReporte;
import ec.com.redepronik.negosys.invfac.service.FacturaService;
import ec.com.redepronik.negosys.rrhh.service.EmpleadoService;

@Controller
@Scope("session")
public class ListadoRetencionVentaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private NotaCreditoBean devolucionBean;

	@Autowired
	private FacturaService facturaService;

	@Autowired
	private EmpleadoService empleadoService;

	private List<Factura> listaFacturas;
	private String criterioBusquedaCliente;
	private String criterioBusquedaNumeroFactura;
	private Date criterioBusquedaFechaDocumento;

	private List<FacturaReporte> listaFacturasDetalle;
	private Factura factura;
	private CantidadFactura cantidadFactura;
	private BigDecimal retRenta;
	private BigDecimal retIva;
	private String mensaje;

	public ListadoRetencionVentaBean() {
		factura = new Factura();
		retRenta = redondearTotales(newBigDecimal());
		retIva = redondearTotales(newBigDecimal());
	}

	public String getCriterioBusquedaNumeroFactura() {
		return criterioBusquedaNumeroFactura;
	}

	public void setCriterioBusquedaNumeroFactura(String criterioBusquedaNumeroFactura) {
		this.criterioBusquedaNumeroFactura = criterioBusquedaNumeroFactura;
	}

	public void generarListaDetalle() {
		if (tipoIdentificacionComprador(factura.getClienteFactura().getPersona().getCedula()).compareTo("04") != 0)
			mensaje = "EN LA FACTURA NO CONSTA UN RUC";
		retIva = factura.getRetIva();
		retRenta = factura.getRetRenta();
		listaFacturasDetalle = new ArrayList<FacturaReporte>();
		cantidadFactura = new CantidadFactura();
		cantidadFactura.settDescuentoEgreso(factura.getDescuento());
		List<DetalleFactura> list = factura.getDetalleFactura();
		for (DetalleFactura de : list) {
			FacturaReporte f = facturaService.asignar(de);
			listaFacturasDetalle.add(f);
			cantidadFactura = facturaService.calcularCantidadFactura(cantidadFactura, f,
					new Date(factura.getFechaInicio().getTime()));
		}
		facturaService.redondearCantidadFactura(cantidadFactura);
	}

	public void insertar() {
		factura.setRetRenta(redondearTotales(retRenta));
		factura.setRetIva(redondearTotales(retIva));
		facturaService.actualizar(factura);
		presentaMensaje(FacesMessage.SEVERITY_INFO, "INGRESO LA RETENCION PARA FACTURA #" + factura.getEstablecimiento()
				+ "-" + factura.getPuntoEmision() + "-" + factura.getSecuencia());
		limpiarInsertar();
	}

	public CantidadFactura getCantidadFactura() {
		return cantidadFactura;
	}

	public String getCriterioBusquedaCliente() {
		return criterioBusquedaCliente;
	}

	public Date getCriterioBusquedaFechaDocumento() {
		return criterioBusquedaFechaDocumento;
	}

	public Factura getFactura() {
		return factura;
	}

	public List<Factura> getListaFacturas() {
		return listaFacturas;
	}

	public List<FacturaReporte> getListaFacturasDetalle() {
		return listaFacturasDetalle;
	}

	public void limpiarObjetos() {
		criterioBusquedaCliente = new String();
		criterioBusquedaFechaDocumento = null;
	}

	public void obtener() {
		listaFacturas = facturaService.obtener(0, criterioBusquedaCliente, criterioBusquedaNumeroFactura, 0,
				criterioBusquedaFechaDocumento, criterioBusquedaFechaDocumento);
		limpiarObjetos();
	}

	public void setCantidadFactura(CantidadFactura cantidadFactura) {
		this.cantidadFactura = cantidadFactura;
	}

	public void setCriterioBusquedaCliente(String criterioBusquedaCliente) {
		this.criterioBusquedaCliente = criterioBusquedaCliente;
	}

	public void setCriterioBusquedaFechaDocumento(Date criterioBusquedaFechaDocumento) {
		this.criterioBusquedaFechaDocumento = criterioBusquedaFechaDocumento;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

	public void setListaFacturas(List<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	public void setListaFacturasDetalle(List<FacturaReporte> listaFacturasDetalle) {
		this.listaFacturasDetalle = listaFacturasDetalle;
	}

	public BigDecimal getRetRenta() {
		return retRenta;
	}

	public void setRetRenta(BigDecimal retRenta) {
		this.retRenta = retRenta;
	}

	public BigDecimal getRetIva() {
		return retIva;
	}

	public void setRetIva(BigDecimal retIva) {
		this.retIva = retIva;
	}

	public void limpiarInsertar() {
		retRenta = redondearTotales(newBigDecimal());
		retIva = redondearTotales(newBigDecimal());
		mensaje = null;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}