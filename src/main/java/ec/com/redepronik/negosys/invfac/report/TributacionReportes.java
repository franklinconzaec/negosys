package ec.com.redepronik.negosys.invfac.report;

import static ec.com.redepronik.negosys.utils.UtilsAplicacion.presentaMensaje;
import static ec.com.redepronik.negosys.utils.UtilsDate.fechaFormatoString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import ec.com.redepronik.negosys.invfac.entity.Retencion;
import ec.com.redepronik.negosys.invfac.entityAux.Anio;
import ec.com.redepronik.negosys.invfac.entityAux.ListRetencionImprimir;
import ec.com.redepronik.negosys.invfac.entityAux.Mes;
import ec.com.redepronik.negosys.invfac.entityAux.Retenciones;
import ec.com.redepronik.negosys.invfac.service.AnexoService;
import ec.com.redepronik.negosys.invfac.service.RetencionService;
import ec.com.redepronik.negosys.invfac.service.RetencionesService;
import ec.com.redepronik.negosys.rrhh.entity.Persona;
import ec.com.redepronik.negosys.rrhh.service.ProveedorService;
import ec.com.redepronik.negosys.utils.anexo.Anexo;
import ec.com.redepronik.negosys.utils.service.ReporteService;

@Controller
@Scope("session")
public class TributacionReportes {

	@Autowired
	private ReporteService reporteService;

	@Autowired
	private RetencionService retencionService;

	@Autowired
	private RetencionesService retencionesService;

	@Autowired
	private AnexoService anexoService;

	@Autowired
	private ProveedorService proveedorService;

	private List<Persona> listaProveedores;
	private Date fechaInicioRetenciones;
	private Date fechaFinRetenciones;
	private Integer proveedorId;

	private Mes mesAnexo;
	private Anio anioAnexo;

	public TributacionReportes() {

	}

	public Mes[] getListaMes() {
		return Mes.values();
	}

	public Anio[] getListaAnio() {
		return Anio.values();
	}

	public Integer getProveedorId() {
		return proveedorId;
	}

	public void setProveedorId(Integer proveedorId) {
		this.proveedorId = proveedorId;
	}

	public List<Persona> getListaProveedores() {
		return listaProveedores;
	}

	public void setListaProveedores(List<Persona> listaProveedores) {
		this.listaProveedores = listaProveedores;
	}

	public Date getFechaInicioRetenciones() {
		return fechaInicioRetenciones;
	}

	public void setFechaInicioRetenciones(Date fechaInicioRetenciones) {
		this.fechaInicioRetenciones = fechaInicioRetenciones;
	}

	public Date getFechaFinRetenciones() {
		return fechaFinRetenciones;
	}

	public void setFechaFinRetenciones(Date fechaFinRetenciones) {
		this.fechaFinRetenciones = fechaFinRetenciones;
	}

	public Mes getMesAnexo() {
		return mesAnexo;
	}

	public void setMesAnexo(Mes mesAnexo) {
		this.mesAnexo = mesAnexo;
	}

	public Anio getAnioAnexo() {
		return anioAnexo;
	}

	public void setAnioAnexo(Anio anioAnexo) {
		this.anioAnexo = anioAnexo;
	}

	@PostConstruct
	public void init() {
		listaProveedores = proveedorService.obtener();
		anioAnexo = Anio.obtenerPorId(fechaFormatoString(new Date()).substring(6, 10));
		mesAnexo = Mes.obtenerPorId(fechaFormatoString(new Date()).substring(3, 5));
	}

	public boolean reporteRetencion(Integer retencionId) {
		List<Retencion> list1 = new ArrayList<Retencion>();
		HashMap<String, Object> map = retencionService.imprimirRetencion(retencionId, list1);
		boolean bn = (boolean) map.get("bn");
		if (bn) {
			List<ListRetencionImprimir> list = new ArrayList<ListRetencionImprimir>();
			list.add(new ListRetencionImprimir(list1));
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("totalRetenido", (BigDecimal) map.get("totalRetenido"));
			reporteService.generarReportePDF(list, parametros, "Retencion");
		}
		return bn;
	}

	public void reporteRetenciones(ActionEvent actionEvent) {
		if (fechaInicioRetenciones == null)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "INGRESE UNA FECHA DE INICIO");
		else if (fechaInicioRetenciones == null)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "INGRESE UNA FECHA DE CORTE");
		else if (fechaInicioRetenciones.compareTo(fechaFinRetenciones) > 0)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "LA FECHA DE INICIO NO PUEDE SER MAYOR A LA FECHA DE CORTE");
		else {
			List<Retenciones> lista = retencionesService.obtener(proveedorId, fechaInicioRetenciones,
					fechaFinRetenciones);
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("fechaInicio", fechaInicioRetenciones);
			parametros.put("fechaFin", fechaFinRetenciones);
			reporteService.generarReportePDF(lista, parametros, "Retenciones");
		}
	}

	public void reporteAnexo(ActionEvent actionEvent) {
		if (anioAnexo == null)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "INGRESE UN AÑO");
		else if (mesAnexo == null)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "INGRESE UN MES");
		else {
			Anexo anexo = anexoService.generarAnexo(anioAnexo, mesAnexo);
			if (anexo != null)
				reporteService.generarReporteXML(Anexo.class, "AT-" + mesAnexo.getId() + anioAnexo.getId(), anexo);
			else
				presentaMensaje(FacesMessage.SEVERITY_ERROR, "EL MES NO ESTA ACTIVO");
		}
	}

}