package ec.com.redepronik.negosys.invfac.dao;

import ec.com.redepronik.negosys.invfac.entityAux.VentasAnexo;
import ec.com.redepronik.negosys.utils.dao.GenericDao;

public interface VentasAnexoDao extends GenericDao<VentasAnexo, Integer> {

}