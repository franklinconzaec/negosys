package ec.com.redepronik.negosys.invfac.service;

import static ec.com.redepronik.negosys.utils.UtilsDate.dateCompleto;
import static ec.com.redepronik.negosys.utils.UtilsDate.fechaFormatoString;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.com.redepronik.negosys.invfac.dao.VolumenVentaFacturaDao;
import ec.com.redepronik.negosys.invfac.entityAux.VolumenVentaFactura;

@Service
public class VolumenVentaFacturaServiceImpl implements VolumenVentaFacturaService {

	@Autowired
	private VolumenVentaFacturaDao volumenVentaFacturaDao;

	public List<VolumenVentaFactura> obtenerVolumenventaFacturaPorConsumidorFinal(int consumidorFinal, Date fechaInicio,
			Date fechaaFin) {
		return volumenVentaFacturaDao.obtenerPorHql("select new VolumenVentaFactura(f.id, f.fechaInicio, "
				+ "f.establecimiento||'-'||f.puntoEmision||'-'||f.secuencia, "
				+ "p.cedula||'-'||p.apellido||' '||p.nombre, "
				+ "(select coalesce(round(sum((df.precioVentaCuadre*df.cantidad)-df.descuentoCuadre),2), cast(0.0 as big_decimal)) "
				+ "from Factura f1 inner join f1.detalleFactura df inner join df.producto pr "
				+ "inner join pr.productosTarifas pt inner join pt.tarifa t where f1.id=f.id "
				+ "and t.id=1 and df.estadoProductoVenta='NR'), "
				+ "(select coalesce(round(sum((df.precioVentaCuadre*df.cantidad)-df.descuentoCuadre),2), cast(0.0 as big_decimal)) "
				+ "from Factura f1 inner join f1.detalleFactura df inner join df.producto pr "
				+ "inner join pr.productosTarifas pt inner join pt.tarifa t where f1.id=f.id "
				+ "and t.id=34 and df.estadoProductoVenta='NR')) from Factura f "
				+ "inner join f.clienteFactura cf inner join cf.persona p "
				+ "where f.activo=true and f.fechaInicio>=?1 and f.fechaInicio<=?2 "
				+ ((consumidorFinal == 0) ? "and p.cedula='9999999999999'"
						: (consumidorFinal == 1) ? "and p.cedula!='9999999999999'" : " ")
				+ " order by f.fechaInicio", new Object[] { fechaInicio, dateCompleto(fechaaFin) });
	}

	public List<VolumenVentaFactura> obtenerPorFechasAndVendedorAndEstadoDocumento(Integer vendedorId,
			Integer estadoDocumento, Date fechaInicio, Date fechaFin, Integer ciudadId) {
		String complementoEstado = estadoDocumento == 0 ? ""
				: estadoDocumento == 1 ? "f.fechaCierre is not null and " : "f.fechaCierre is null and ";
		String complementoCiudad = ciudadId == 0 ? "" : vendedorId == 0 ? "and c.id=?3 " : "and c.id=?4 ";
		List<VolumenVentaFactura> list = null;
		if (vendedorId == 0)
			list = volumenVentaFacturaDao.obtenerPorHql(
					"select new VolumenVentaFactura(f.id, "
							+ "f.fechaInicio, f.establecimiento||'-'||f.puntoEmision||'-'||f.secuencia, "
							+ "p.cedula||'-'||p.apellido||' '||p.nombre, "
							+ "(select coalesce(round(sum((df.precioVentaCuadre*df.cantidad)-df.descuentoCuadre),2), cast(0.00 as big_decimal)) "
							+ "from Factura f1 inner join f1.detalleFactura df inner join df.producto pr "
							+ "inner join pr.productosTarifas pt inner join pt.tarifa t where f1.id=f.id "
							+ "and t.id=1 and df.estadoProductoVenta='NR'), "
							+ "(select coalesce(round(sum((df.precioVentaCuadre*df.cantidad)-df.descuentoCuadre),2), cast(0.00 as big_decimal)) "
							+ "from Factura f1 inner join f1.detalleFactura df inner join df.producto pr "
							+ "inner join pr.productosTarifas pt inner join pt.tarifa t where f1.id=f.id "
							+ "and t.id=34 and df.estadoProductoVenta='NR')) from Factura f "
							+ "inner join f.clienteFactura cf inner join cf.persona p inner join p.ciudad c "
							+ "inner join f.vendedor v where " + complementoEstado
							+ "f.activo=true and f.fechaInicio>=?1 " + "and f.fechaInicio<=?2 " + complementoCiudad
							+ "order by f.establecimiento||'-'||f.puntoEmision||'-'||f.secuencia",
					new Object[] { fechaInicio, dateCompleto(fechaFin), ciudadId == 0 ? null : ciudadId });
		else
			list = volumenVentaFacturaDao.obtenerPorHql(
					"select new VolumenVentaFactura(f.id, "
							+ "f.fechaInicio, f.establecimiento||'-'||f.puntoEmision||'-'||f.secuencia, "
							+ "p.cedula||'-'||p.apellido||' '||p.nombre, "
							+ "(select coalesce(round(sum((df.precioVentaCuadre*df.cantidad)-df.descuentoCuadre),2), cast(0.00 as big_decimal)) "
							+ "from Factura f1 inner join f1.detalleFactura df inner join df.producto pr "
							+ "inner join pr.productosTarifas pt inner join pt.tarifa t where f1.id=f.id "
							+ "and t.id=1 and df.estadoProductoVenta='NR'), "
							+ "(select coalesce(round(sum((df.precioVentaCuadre*df.cantidad)-df.descuentoCuadre),2), cast(0.00 as big_decimal)) "
							+ "from Factura f1 inner join f1.detalleFactura df inner join df.producto pr "
							+ "inner join pr.productosTarifas pt inner join pt.tarifa t where f1.id=f.id "
							+ "and t.id=34 and df.estadoProductoVenta='NR')) from Factura f "
							+ "inner join f.clienteFactura cf inner join cf.persona p inner join p.ciudad c "
							+ "inner join f.vendedor v where " + complementoEstado + "f.activo=true "
							+ "and v.id=?1 and f.fechaInicio>=?2 and f.fechaInicio<=?3 " + complementoCiudad
							+ "order by f.establecimiento||'-'||f.puntoEmision||'-'||f.secuencia",
					new Object[] { vendedorId, fechaInicio, dateCompleto(fechaFin), ciudadId == 0 ? null : ciudadId });
		return list;
	}

	public List<VolumenVentaFactura> obtenerPorFechasAndVendedorAndProveedorAndEstadoDocumento(Integer vendedorId,
			Integer estadoDocumento, Date fechaInicio, Date fechaFin, Integer proveedorId) {
		List<VolumenVentaFactura> list = null;
		if (vendedorId == 0)
			list = volumenVentaFacturaDao
					.obtenerPorSql("SELECT * from negosys.fun_reporte_volumen_venta_factura_proveedor('"
							+ fechaFormatoString(fechaInicio, "yyyy-MM-dd") + "', '"
							+ fechaFormatoString(dateCompleto(fechaFin), "yyyy-MM-dd HH:mm:ss") + "', "
							+ estadoDocumento + ", " + proveedorId + ", null);", VolumenVentaFactura.class);
		else
			list = volumenVentaFacturaDao.obtenerPorSql(
					"SELECT * from negosys.fun_reporte_volumen_venta_factura_proveedor('"
							+ fechaFormatoString(fechaInicio, "yyyy-MM-dd") + "', '"
							+ fechaFormatoString(dateCompleto(fechaFin), "yyyy-MM-dd HH:mm:ss") + "', "
							+ estadoDocumento + ", " + proveedorId + ", " + vendedorId + ");",
					VolumenVentaFactura.class);
		return list;
	}
}