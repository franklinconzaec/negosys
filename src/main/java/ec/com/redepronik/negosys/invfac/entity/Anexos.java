package ec.com.redepronik.negosys.invfac.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "anexos")
public class Anexos implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String anio;
	private String mes;
	private String anexo;
	private Timestamp fecha;
	private BigDecimal tventa0;
	private BigDecimal tventa12;
	private BigDecimal tcompra0;
	private BigDecimal tcompra12;

	public Anexos() {
	}

	public Anexos(Integer id, String anio, String mes, String anexo, Timestamp fecha, BigDecimal tventa0,
			BigDecimal tventa12, BigDecimal tcompra0, BigDecimal tcompra12) {
		this.id = id;
		this.anio = anio;
		this.mes = mes;
		this.anexo = anexo;
		this.fecha = fecha;
		this.tventa0 = tventa0;
		this.tventa12 = tventa12;
		this.tcompra0 = tcompra0;
		this.tcompra12 = tcompra12;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Anexos other = (Anexos) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Id
	@SequenceGenerator(allocationSize = 1, name = "ANEXOS_ID_GENERATOR", sequenceName = "ANEXOS_ID_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ANEXOS_ID_GENERATOR")
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	@Column(length = 4, nullable = false)
	public String getAnio() {
		return anio;
	}

	@Column(length = 2, nullable = false)
	public String getMes() {
		return mes;
	}

	@Column(nullable = false)
	public String getAnexo() {
		return anexo;
	}

	@Column(nullable = false)
	public Timestamp getFecha() {
		return fecha;
	}

	@Column(nullable = false, precision = 8, scale = 2)
	public BigDecimal getTventa0() {
		return tventa0;
	}

	@Column(nullable = false, precision = 8, scale = 2)
	public BigDecimal getTventa12() {
		return tventa12;
	}

	@Column(nullable = false, precision = 8, scale = 2)
	public BigDecimal getTcompra0() {
		return tcompra0;
	}

	@Column(nullable = false, precision = 8, scale = 2)
	public BigDecimal getTcompra12() {
		return tcompra12;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public void setTventa0(BigDecimal tventa0) {
		this.tventa0 = tventa0;
	}

	public void setTventa12(BigDecimal tventa12) {
		this.tventa12 = tventa12;
	}

	public void setTcompra0(BigDecimal tcompra0) {
		this.tcompra0 = tcompra0;
	}

	public void setTcompra12(BigDecimal tcompra12) {
		this.tcompra12 = tcompra12;
	}

}