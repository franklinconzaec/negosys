package ec.com.redepronik.negosys.invfac.controller;

import static ec.com.redepronik.negosys.utils.UtilsAplicacion.presentaMensaje;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import ec.com.redepronik.negosys.invfac.entity.Anulado;
import ec.com.redepronik.negosys.invfac.entity.TipoComprobante;
import ec.com.redepronik.negosys.invfac.service.AnuladoService;

@Controller
@Scope("session")
public class ListadoAnuladoBean {

	@Autowired
	private AnuladoService anuladoService;

	private List<Anulado> listaAnulados;
	private Anulado anulado;
	private TipoComprobante criterioBusquedaTipoComprobante;
	private String criterioBusquedaNumeroComprobante;
	private Date criterioBusquedaFecha;

	public ListadoAnuladoBean() {
	}

	public TipoComprobante[] getListaComprobantes() {
		return TipoComprobante.values();
	}

	public List<Anulado> getListaAnulados() {
		return listaAnulados;
	}

	public void setListaAnulados(List<Anulado> listaAnulados) {
		this.listaAnulados = listaAnulados;
	}

	public Anulado getAnulado() {
		return anulado;
	}

	public void setAnulado(Anulado anulado) {
		this.anulado = anulado;
	}

	public TipoComprobante getCriterioBusquedaTipoComprobante() {
		return criterioBusquedaTipoComprobante;
	}

	public void setCriterioBusquedaTipoComprobante(TipoComprobante criterioBusquedaTipoComprobante) {
		this.criterioBusquedaTipoComprobante = criterioBusquedaTipoComprobante;
	}

	public String getCriterioBusquedaNumeroComprobante() {
		return criterioBusquedaNumeroComprobante;
	}

	public void setCriterioBusquedaNumeroComprobante(String criterioBusquedaNumeroComprobante) {
		this.criterioBusquedaNumeroComprobante = criterioBusquedaNumeroComprobante;
	}

	public Date getCriterioBusquedaFecha() {
		return criterioBusquedaFecha;
	}

	public void setCriterioBusquedaFecha(Date criterioBusquedaFecha) {
		this.criterioBusquedaFecha = criterioBusquedaFecha;
	}

	@PostConstruct
	public void init() {
		anulado = new Anulado();
	}

	public void obtener() {
		listaAnulados = anuladoService.obtener(criterioBusquedaTipoComprobante, criterioBusquedaNumeroComprobante,
				criterioBusquedaFecha);
	}

	public void insertar() {
		if (anulado.getTipoComprobante() == null)
			presentaMensaje(FacesMessage.SEVERITY_WARN, "ESCOJA UN TIPO DE COMPROBANTE", "cerrar", false);
		else if (anulado.getEstablecimiento().compareTo("") == 0)
			presentaMensaje(FacesMessage.SEVERITY_WARN, "INGRESE UN ESTABLECIMIENTO");
		else if (anulado.getPuntoEmision().compareTo("") == 0)
			presentaMensaje(FacesMessage.SEVERITY_WARN, "INGRESE UN PTO. EMISION");
		else if (anulado.getSecuenciaInicio().compareTo("") == 0)
			presentaMensaje(FacesMessage.SEVERITY_WARN, "INGRESE UNA SECUENCIA DE INICIO");
		else if (anulado.getSecuenciaFin().compareTo("") == 0)
			presentaMensaje(FacesMessage.SEVERITY_WARN, "INGRESE UNA SECUENCIA DE FIN");
		else if (anulado.getAutorizacion().compareTo("") == 0)
			presentaMensaje(FacesMessage.SEVERITY_WARN, "INGRESE EL NUMERO DE AUTORIZACION", "cerrar", false);
		else {
			if (anuladoService.insertar(anulado)) {
				presentaMensaje(FacesMessage.SEVERITY_INFO,
						"SE AGREGO CORRECTAMENTE EL COMPROBANTE " + anulado.getTipoComprobante().getNombre(), "cerrar",
						true);
				anulado = new Anulado();
			}
		}
	}

	public void pasarSecuencia() {
		anulado.setSecuenciaInicio(String.format("%09d", Integer.parseInt(anulado.getSecuenciaInicio())));
		anulado.setSecuenciaFin(String.format("%09d", Integer.parseInt(anulado.getSecuenciaInicio())));
	}

}