package ec.com.redepronik.negosys.invfac.service;

import static ec.com.redepronik.negosys.utils.UtilsAplicacion.presentaMensaje;
import static ec.com.redepronik.negosys.utils.UtilsDate.dateCompleto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.com.redepronik.negosys.invfac.dao.VolumenVentaProductoDao;
import ec.com.redepronik.negosys.invfac.entity.EstadoProductoVenta;
import ec.com.redepronik.negosys.invfac.entity.ProductoUnidad;
import ec.com.redepronik.negosys.invfac.entityAux.VolumenVentaProducto;

@Service
public class VolumenVentaProductoServiceImpl implements VolumenVentaProductoService {

	@Autowired
	private ProductoService productoService;

	@Autowired
	private VolumenVentaProductoDao volumenVentaProductoDao;

	public List<VolumenVentaProducto> obtenerVolumenVentaProducto(Date fechaInicio, Date fechaFin, int ordenarId,
			EstadoProductoVenta estadoProductoVenta) {
		List<VolumenVentaProducto> list = new ArrayList<VolumenVentaProducto>();
		if (fechaInicio == null && fechaFin == null && estadoProductoVenta == null)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "INGRESE UN CRITERIO DE BUSQUEDA");
		else {
			list = volumenVentaProductoDao.obtenerPorHql(
					"select new VolumenVentaProducto(p.id, p.nombre, sum(df.cantidad), "
							+ "round(sum((df.cantidad*df.precioVentaCuadre)-(df.cantidad*df.precioCosto)),2)) "
							+ "from DetalleFactura df inner join df.factura f inner join df.producto p "
							+ "where f.activo=true and f.fechaInicio>=?1 and f.fechaInicio<=?2 "
							+ (estadoProductoVenta == null ? "" : "and df.estadoProductoVenta=?3 ")
							+ "group by p.id, p.nombre order by "
							+ (ordenarId == 1 ? "p.nombre" : "sum(df.cantidad) desc"),
					new Object[] { fechaInicio, dateCompleto(fechaFin),
							estadoProductoVenta == null ? null : estadoProductoVenta });
			if (list.isEmpty())
				presentaMensaje(FacesMessage.SEVERITY_INFO, "NO SE ENCONTRARON COINCIDENCIAS");
		}
		cantidadString(list);
		return list;
	}

	public List<VolumenVentaProducto> obtenerVolumenVentaProductoPorVendedor(Date fechaInicio, Date fechaFin,
			int proveedorId, int vendedorId) {
		List<VolumenVentaProducto> list = new ArrayList<VolumenVentaProducto>();
		if (fechaInicio == null && fechaFin == null)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "INGRESE UN CRITERIO DE BUSQUEDA");
		else {
			if (proveedorId != 0) {
				list = volumenVentaProductoDao.obtenerPorHql(
						"select distinct new VolumenVentaProducto(p.id, p.nombre, "
								+ "(coalesce((select sum(df.cantidad) from Producto pr "
								+ "inner join pr.detalleFactura df inner join df.factura f "
								+ "inner join f.vendedor v where f.activo=true "
								+ "and f.fechaInicio>=?2 and f.fechaInicio<=?3 "
								+ (vendedorId == 0 ? "" : "and v.id=?4 ") + "and pr.id=p.id), 0))) from Producto p "
								+ "inner join p.detalleIngresos di inner join di.ingreso i "
								+ "inner join i.proveedor pr where pr.id=?1 order by p.nombre desc",
						new Object[] { proveedorId, fechaInicio, dateCompleto(fechaFin),
								vendedorId == 0 ? null : vendedorId });
				List<VolumenVentaProducto> list1 = new ArrayList<VolumenVentaProducto>();
				list = quicksort(list, 0, list.size() - 1);
				for (int i = list.size() - 1; i >= 0; i--) {
					VolumenVentaProducto vvp = list.get(i);
					if (vvp.getCantidad() != null && vvp.getCantidad() != 0)
						list1.add(vvp);
				}
				list = list1;
			} else if (proveedorId == 0)
				list = volumenVentaProductoDao.obtenerPorHql(
						"select distinct new VolumenVentaProducto(p.id, p.nombre, sum(df.cantidad)) "
								+ "from DetalleFactura df inner join df.factura f inner join f.vendedor v "
								+ "inner join df.producto p where f.activo=true "
								+ "and f.fechaInicio>=?1 and f.fechaInicio<=?2 "
								+ (vendedorId == 0 ? "" : "and v.id=?3 ") + "group by p.id, p.nombre "
								+ "order by sum(df.cantidad) desc",
						new Object[] { fechaInicio, dateCompleto(fechaFin), vendedorId == 0 ? null : vendedorId });
			if (list.isEmpty())
				presentaMensaje(FacesMessage.SEVERITY_INFO, "NO SE ENCONTRARON COINCIDENCIAS");
		}
		cantidadString(list);
		return list;
	}

	private void cantidadString(List<VolumenVentaProducto> list) {
		if (!list.isEmpty())
			for (VolumenVentaProducto vvp : list) {
				List<ProductoUnidad> listaUnidades = productoService.obtenerUnidadesPorProductoId(vvp.getId())
						.getProductoUnidads();
				vvp.setCantidadString("(" + vvp.getCantidad() + listaUnidades.get(0).getUnidad().getAbreviatura() + ") "
						+ productoService.convertirUnidadString(vvp.getCantidad().intValue(), listaUnidades));
			}
	}

	private List<VolumenVentaProducto> quicksort(List<VolumenVentaProducto> list, int iz, int de) {
		if (iz >= de)
			return list;
		int i = iz;
		int d = de;
		if (iz != de) {
			VolumenVentaProducto epr;
			int pivote = iz;
			while (iz != de) {
				while (list.get(de).getCantidad() >= list.get(pivote).getCantidad() && i < de)
					de--;
				while (list.get(iz).getCantidad() < list.get(pivote).getCantidad() && i < de)
					iz++;
				if (de != iz) {
					epr = list.get(de);
					list.set(de, list.get(iz));
					list.set(iz, epr);
				}
				if (iz == de) {
					quicksort(list, i, iz - 1);
					quicksort(list, iz + 1, d);
				}
			}
		}
		return list;
	}

}