package ec.com.redepronik.negosys.invfac.entityAux;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class EntregaProductoReporte implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private long ean;
	private String nombre;
	private long cantidad;
	private String cantidadString;

	public EntregaProductoReporte() {

	}

	public EntregaProductoReporte(long ean, String nombre, long cantidad) {
		this.ean = ean;
		this.nombre = nombre;
		this.cantidad = cantidad;
	}

	public EntregaProductoReporte(long ean, String nombre, int cantidad, String cantidadString) {
		this.ean = ean;
		this.nombre = nombre;
		this.cantidad = cantidad;
		this.cantidadString = cantidadString;
	}

	public long getEan() {
		return ean;
	}

	public void setEan(long ean) {
		this.ean = ean;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public long getCantidad() {
		return cantidad;
	}

	public void setCantidad(long cantidad) {
		this.cantidad = cantidad;
	}

	public String getCantidadString() {
		return cantidadString;
	}

	public void setCantidadString(String cantidadString) {
		this.cantidadString = cantidadString;
	}

}
