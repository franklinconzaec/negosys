package ec.com.redepronik.negosys.invfac.service;

import static ec.com.redepronik.negosys.utils.UtilsAplicacion.presentaMensaje;
import static ec.com.redepronik.negosys.utils.UtilsDate.dateCompleto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.com.redepronik.negosys.invfac.dao.AnuladoDao;
import ec.com.redepronik.negosys.invfac.entity.Anulado;
import ec.com.redepronik.negosys.invfac.entity.TipoComprobante;

@Service
public class AnuladoServiceImpl implements AnuladoService {

	@Autowired
	private AnuladoDao anuladoDao;

	public boolean insertar(Anulado anulado) {
		anulado.setEstablecimiento(String.format("%03d", Integer.parseInt(anulado.getEstablecimiento())));
		anulado.setPuntoEmision(String.format("%03d", Integer.parseInt(anulado.getPuntoEmision())));
		anulado.setSecuenciaInicio(String.format("%09d", Integer.parseInt(anulado.getSecuenciaInicio())));
		anulado.setSecuenciaFin(String.format("%09d", Integer.parseInt(anulado.getSecuenciaFin())));
		anulado.setFechaAnulado(new Date());
		boolean bn = comprobarAnulado(anulado);
		if (bn)
			anuladoDao.insertar(anulado);
		else
			presentaMensaje(FacesMessage.SEVERITY_INFO, "EL ANULADO YA ESTA REGISTRADO", "error", true);
		return bn;
	}

	public boolean comprobarAnulado(Anulado a) {
		List<Anulado> list = anuladoDao.obtenerPorHql(
				"select a from Anulado a " + "where a.tipoComprobante=?1 and " + "a.establecimiento=?2 and "
						+ "a.puntoEmision=?3 and ( " + "(a.secuenciaInicio>=?4 and " + "a.secuenciaInicio<=?5) or "
						+ "(a.secuenciaFin>=?4 and " + "a.secuenciaFin<=?5)) order by a.fechaAnulado",
				new Object[] { a.getTipoComprobante(), a.getEstablecimiento(), a.getPuntoEmision(),
						a.getSecuenciaInicio(), a.getSecuenciaFin() });
		if (list.isEmpty())
			return true;
		return false;
	}

	public Anulado obtenerPorId(Integer id) {
		List<Anulado> list = anuladoDao.obtenerPorHql(
				"select a from Anulado a " + "where a.id=?1 order by a.fechaAnulado", new Object[] { id });
		if (list == null || list.isEmpty())
			return null;
		else
			return list.get(0);
	}

	public List<Anulado> obtener(TipoComprobante criterioBusquedaTipoComprobante,
			String criterioBusquedaNumeroComprobante, Date criterioBusquedaFecha) {
		List<Anulado> list = new ArrayList<Anulado>();
		if (criterioBusquedaTipoComprobante == null && criterioBusquedaNumeroComprobante.compareToIgnoreCase("") == 0
				&& criterioBusquedaFecha == null)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "INGRESE UN CRITERIO DE BUSQUEDA");
		else if (criterioBusquedaNumeroComprobante.length() >= 1 && criterioBusquedaNumeroComprobante.length() <= 2)
			presentaMensaje(FacesMessage.SEVERITY_ERROR,
					"INGRESE MAS DE 2 CARACTERES PARA LA BÚSQUEDA POR NÚMERO DE COMPROBANTES");
		else if (!criterioBusquedaNumeroComprobante.matches("[0-9]*"))
			presentaMensaje(FacesMessage.SEVERITY_ERROR,
					"INGRESE SOLO NÚMEROS PARA LA BÚSQUEDA POR NÚMERO DE COMPROBANTES");
		else {
			if (criterioBusquedaTipoComprobante != null)
				list = anuladoDao.obtenerPorHql(
						"select a from Anulado a " + "where a.tipoComprobante=?1 " + "order by a.fechaAnulado",
						new Object[] { criterioBusquedaTipoComprobante });
			else if (criterioBusquedaNumeroComprobante.compareTo("") != 0)
				list = anuladoDao.obtenerPorHql(
						"select a from Anulado a " + "where a.establecimiento like ?1 or "
								+ "a.puntoEmision like ?1 or " + "(a.secuenciaInicio<=?2 and "
								+ "a.secuenciaFin>=?2) order by a.fechaAnulado",
						new Object[] { "%" + criterioBusquedaNumeroComprobante + "%",
								String.format("%09d", Integer.parseInt(criterioBusquedaNumeroComprobante)) });
			else if (criterioBusquedaFecha != null)
				list = anuladoDao.obtenerPorHql(
						"select a from Anulado a " + "where a.fechaAnulado>=?1 and a.fechaAnulado<=?2 "
								+ "order by a.fechaAnulado",
						new Object[] { criterioBusquedaFecha, dateCompleto(criterioBusquedaFecha) });
			if (list.isEmpty())
				presentaMensaje(FacesMessage.SEVERITY_INFO, "NO SE ENCONTRARON COINCIDENCIAS");
		}
		return list;
	}

	public List<Anulado> obtenerParaAnexo(Date fechaInicio, Date fechaFin) {
		List<Anulado> list = new ArrayList<Anulado>();
		if (fechaFin == null && fechaFin == null)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "INGRESE UN CRITERIO DE BUSQUEDA");
		else
			list = anuladoDao.obtenerPorHql("select a from Anulado a "
					+ "where a.fechaAnulado>=?1 and a.fechaAnulado<=?2 " + "order by a.fechaAnulado",
					new Object[] { fechaInicio, dateCompleto(fechaFin) });
		if (list.isEmpty())
			presentaMensaje(FacesMessage.SEVERITY_INFO, "NO SE ENCONTRARON COINCIDENCIAS");
		return list;
	}
}