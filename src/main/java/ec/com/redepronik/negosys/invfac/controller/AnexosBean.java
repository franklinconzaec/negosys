package ec.com.redepronik.negosys.invfac.controller;

import static ec.com.redepronik.negosys.utils.UtilsAplicacion.presentaMensaje;

import java.io.IOException;
import java.util.List;

import javax.faces.application.FacesMessage;

import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import ec.com.redepronik.negosys.invfac.entity.Anexos;
import ec.com.redepronik.negosys.invfac.entityAux.Anio;
import ec.com.redepronik.negosys.invfac.entityAux.Mes;
import ec.com.redepronik.negosys.invfac.service.AnexosService;
import ec.com.redepronik.negosys.utils.anexo.Anexo;
import ec.com.redepronik.negosys.utils.service.ReporteService;

@Controller
@Scope("session")
public class AnexosBean {

	@Autowired
	private AnexosService anexosService;

	@Autowired
	private ReporteService reporteService;

	private List<Anexos> listaAnexos;
	private Anexos anexos;
	private Mes mesAnexo;
	private Anio anioAnexo;

	public Mes[] getListaMes() {
		return Mes.values();
	}

	public Anio[] getListaAnio() {
		return Anio.values();
	}

	public void obtener() {
		listaAnexos = anexosService.obtenerPorMes(anioAnexo, mesAnexo);
	}

	public void insertar(FileUploadEvent event) {
		try {
			anexosService.insertar(event.getFile().getInputstream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void eliminar() {
		anexosService.eliminar(anexos);
		obtener();
	}

	public void generar() {
		Anexo anexo = anexosService.generarAnexoFinal(anioAnexo, mesAnexo);
		if (anexo != null)
			reporteService.generarReporteXML(Anexo.class, "AT-" + mesAnexo.getId() + anioAnexo.getId(), anexo);
		else
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "EL MES NO ESTA ACTIVO");
	}

	public List<Anexos> getListaAnexos() {
		return listaAnexos;
	}

	public void setListaAnexos(List<Anexos> listaAnexos) {
		this.listaAnexos = listaAnexos;
	}

	public Anexos getAnexos() {
		return anexos;
	}

	public void setAnexos(Anexos anexos) {
		this.anexos = anexos;
	}

	public Mes getMesAnexo() {
		return mesAnexo;
	}

	public void setMesAnexo(Mes mesAnexo) {
		this.mesAnexo = mesAnexo;
	}

	public Anio getAnioAnexo() {
		return anioAnexo;
	}

	public void setAnioAnexo(Anio anioAnexo) {
		this.anioAnexo = anioAnexo;
	}

}
