package ec.com.redepronik.negosys.invfac.report;

import static ec.com.redepronik.negosys.utils.UtilsAplicacion.presentaMensaje;
import static ec.com.redepronik.negosys.utils.UtilsDate.fechaFormatoDate;
import static ec.com.redepronik.negosys.utils.UtilsMath.iva;
import static ec.com.redepronik.negosys.utils.UtilsMath.newBigDecimal;
import static ec.com.redepronik.negosys.utils.UtilsMath.parametro;
import static ec.com.redepronik.negosys.utils.UtilsMath.redondearTotales;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import ec.com.redepronik.negosys.invfac.entity.Cotizacion;
import ec.com.redepronik.negosys.invfac.entity.EstadoProductoVenta;
import ec.com.redepronik.negosys.invfac.entity.Factura;
import ec.com.redepronik.negosys.invfac.entity.Impuesto;
import ec.com.redepronik.negosys.invfac.entity.Local;
import ec.com.redepronik.negosys.invfac.entity.Producto;
import ec.com.redepronik.negosys.invfac.entity.ProductoUnidad;
import ec.com.redepronik.negosys.invfac.entityAux.CierreCajaCajero;
import ec.com.redepronik.negosys.invfac.entityAux.CotizacionImprimir;
import ec.com.redepronik.negosys.invfac.entityAux.EgresoCaja;
import ec.com.redepronik.negosys.invfac.entityAux.EntregaProductoReporte;
import ec.com.redepronik.negosys.invfac.entityAux.FacturaImprimir;
import ec.com.redepronik.negosys.invfac.entityAux.GananciaFacturaReporte;
import ec.com.redepronik.negosys.invfac.entityAux.IngresoCaja;
import ec.com.redepronik.negosys.invfac.entityAux.ListFacturaImprimir;
import ec.com.redepronik.negosys.invfac.entityAux.PersonaCedulaNombre;
import ec.com.redepronik.negosys.invfac.entityAux.RefrigeradorCliente;
import ec.com.redepronik.negosys.invfac.entityAux.ReporteDialtor;
import ec.com.redepronik.negosys.invfac.entityAux.VolumenVentaFactura;
import ec.com.redepronik.negosys.invfac.entityAux.VolumenVentaFacturaVendedor;
import ec.com.redepronik.negosys.invfac.entityAux.VolumenVentaProducto;
import ec.com.redepronik.negosys.invfac.entityAux.VolumenVentaProductoCliente;
import ec.com.redepronik.negosys.invfac.entityAux.VolumenVentaProductoVendedor;
import ec.com.redepronik.negosys.invfac.service.CotizacionService;
import ec.com.redepronik.negosys.invfac.service.FacturaService;
import ec.com.redepronik.negosys.invfac.service.NotaCreditoService;
import ec.com.redepronik.negosys.invfac.service.ProductoService;
import ec.com.redepronik.negosys.invfac.service.TarifaService;
import ec.com.redepronik.negosys.invfac.service.VolumenVentaFacturaService;
import ec.com.redepronik.negosys.invfac.service.VolumenVentaProductoService;
import ec.com.redepronik.negosys.rrhh.entity.Ciudad;
import ec.com.redepronik.negosys.rrhh.entity.Cliente;
import ec.com.redepronik.negosys.rrhh.entity.Persona;
import ec.com.redepronik.negosys.rrhh.entity.Provincia;
import ec.com.redepronik.negosys.rrhh.service.CiudadService;
import ec.com.redepronik.negosys.rrhh.service.ClienteService;
import ec.com.redepronik.negosys.rrhh.service.EmpleadoService;
import ec.com.redepronik.negosys.rrhh.service.PersonaService;
import ec.com.redepronik.negosys.rrhh.service.ProveedorService;
import ec.com.redepronik.negosys.utils.service.ReporteService;

@Controller
@Scope("session")
public class FacturacionReportes {

	@Autowired
	private CiudadService ciudadService;
	@Autowired
	private ClienteService clienteService;
	@Autowired
	private CotizacionService cotizacionService;
	@Autowired
	private FacturaService egresoService;
	@Autowired
	private EmpleadoService empleadoService;
	@Autowired
	private NotaCreditoService notaCreditoService;
	@Autowired
	private PersonaService personaService;
	@Autowired
	private ProductoService productoService;
	@Autowired
	private ProveedorService proveedorService;
	@Autowired
	private ReporteService reporteService;
	@Autowired
	private TarifaService tarifaService;
	@Autowired
	private VolumenVentaFacturaService volumenVentaFacturaService;
	@Autowired
	private VolumenVentaProductoService volumenVentaProductoService;

	private int cajero;

	private Ciudad ciudad;

	private int ciudadId;

	private int ciudadIdVVF;

	private EstadoProductoVenta estado;
	private int estadoDocumento;
	private Date fechaCierreCaja;
	private Date fechaFin;

	private Date fechaFinVVPPC;
	private Date fechaInicio;
	private Date fechaInicioVVPPC;
	private boolean gananciaBrutaNeta;
	// //cierre caja
	private List<PersonaCedulaNombre> listaCajeros;
	private List<Ciudad> listaCiudades;

	private List<Ciudad> listaCiudadesVVF;
	private List<Ciudad> listaCiudadesVVPPC;
	private List<Cliente> listaClientesVVPPC;

	private List<Factura> listaFacturas;
	private List<Factura> listaFacturaSeleccionado;
	private List<Factura> listaFacturasImprimir;
	private List<Persona> listaProveedores;
	private List<PersonaCedulaNombre> listaVendedores;

	private int[] ciudadVector;

	private int clienteId;

	private Local local;

	private int consumidorFinal;
	private boolean disabledCajero;
	private int ordenarId;

	private int proveedorId;

	private Provincia provincia;
	private int secuenciaFin;
	private int secuenciaInicio;
	private int vendedorId;

	@PostConstruct
	public void init() {
		listaVendedores = empleadoService.obtenerPorCargo(5);
		listaCajeros = empleadoService.obtenerPorCargo(4);
		listaProveedores = proveedorService.obtenerActivos();
		listaCiudadesVVPPC = ciudadService.obtener(true);
		listaCiudadesVVF = ciudadService.obtener(true);

		setDisabledCajero(false);
		if (!personaService.comprobarRol(SecurityContextHolder.getContext().getAuthentication().getName(), "ADMI")) {
			cajero = empleadoService.obtenerEmpleadoCargoPorCedulaAndCargo(
					SecurityContextHolder.getContext().getAuthentication().getName(), 4).getId();
			setDisabledCajero(true);
		}

		fechaCierreCaja = new Date();

		listaFacturasImprimir = new ArrayList<Factura>();
		listaFacturaSeleccionado = new ArrayList<Factura>();
		ciudad = new Ciudad();
		listaCiudades = new ArrayList<Ciudad>();
		local = new Local();
		fechaInicioVVPPC = new Date();
		fechaFinVVPPC = new Date();
		gananciaBrutaNeta = false;
	}

	public void cargarCiudades() {
		if (provincia.getId() != 0) {
			listaCiudades = new ArrayList<Ciudad>();
			listaCiudades = ciudadService.obtenerPorProvincia(provincia);
			ciudadVector = null;
		}
	}

	public void cargarClienteProveedorCiudadFechas() {
		if (proveedorId != 0 && ciudadId != 0)
			listaClientesVVPPC = clienteService.obtenerPorProveedorPorCiudad(proveedorId, ciudadId, fechaInicioVVPPC,
					fechaFinVVPPC);
	}

	public void onDateSelect(SelectEvent event) {
		fechaInicioVVPPC = fechaFormatoDate((Date) event.getObject());
	}

	public void onDateSelect1(SelectEvent event) {
		fechaFinVVPPC = fechaFormatoDate((Date) event.getObject());
	}

	public void limpiarObjetosBusqueda() {
		fechaInicio = null;
		fechaFin = null;
		ciudad = new Ciudad();
		local = new Local();
		listaFacturas = new ArrayList<Factura>();
	}

	public void limpiarObjetosBusquedaImprimir() {
		listaFacturasImprimir = new ArrayList<Factura>();
		secuenciaInicio = 0;
		secuenciaFin = 0;
		local = new Local();
	}

	public void obtenerFacturas() {
		if (fechaInicio == null)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "INGRESE UNA FECHA DE INICIO");
		else if (fechaFin == null)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "INGRESE UNA FECHA DE FIN");
		else if (local.getCodigoEstablecimiento().compareToIgnoreCase("") == 0)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "ESCOJA UN LOCAL");
		else if (provincia.getId() == 0)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "ESCOJA UNA PROVINCIA");
		else if (ciudadVector.length == 0)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "ESCOJA UNA CIUDAD");
		else
			listaFacturas = egresoService.obtenerEntregaProducto(fechaInicio, fechaFin, local, ciudadVector);
	}

	public void obtenerFacturasImprimir() {
		if (secuenciaInicio == 0)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "Ingrese una Secuencia de Inicio");
		else if (secuenciaFin == 0)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "Ingrese una Secuencia de Fin");
		else if (local.getCodigoEstablecimiento().compareToIgnoreCase("") == 0)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "Escoja un Local");
		else
			listaFacturasImprimir = egresoService.obtenerFacturasImprimir(secuenciaInicio, secuenciaFin, local);
	}

	public void imprimirFacturasLote() {
		if (parametro.getRuc().compareToIgnoreCase("0704812130001") == 0) {
			List<FacturaImprimir> list = new ArrayList<FacturaImprimir>();
			for (Factura f : listaFacturasImprimir)
				egresoService.imprimirFactura(f.getId(), list);
			reporteService.generarReportePDF(list, new HashMap<String, Object>(), "Factura");
		} else {
			List<ListFacturaImprimir> list = new ArrayList<ListFacturaImprimir>();
			for (Factura f : listaFacturasImprimir) {
				List<FacturaImprimir> list1 = new ArrayList<FacturaImprimir>();
				boolean bn = egresoService.imprimirFactura(f.getId(), list1);
				if (bn)
					list.add(new ListFacturaImprimir(list1));
			}
			reporteService.generarReportePDF(list, new HashMap<String, Object>(), "FacturaC");
		}
	}

	@PreAuthorize("hasAnyAuthority('ADMI', 'CAJA')")
	public void reporteCierreCaja(ActionEvent actionEvent) {
		List<CierreCajaCajero> list = new ArrayList<CierreCajaCajero>();
		if (cajero == 0)
			for (PersonaCedulaNombre c : listaCajeros) {
				List<IngresoCaja> listIngresoCaja = egresoService.reporteIngresoCaja(c.getId(), fechaCierreCaja);
				List<EgresoCaja> listEgresoCaja = notaCreditoService.reporteEgresoCaja(c.getId(), fechaCierreCaja);
				if (!listIngresoCaja.isEmpty() || !listEgresoCaja.isEmpty()) {
					BigDecimal totalIngreso = newBigDecimal();
					BigDecimal totalEgreso = newBigDecimal();
					for (IngresoCaja ic : listIngresoCaja) {
						ic.setTotalFactura(egresoService.calcularTotal(ic.getCodigoDocumento()));
						// ic.setPago(ic.getTotalFactura());
						totalIngreso = totalIngreso.add(ic.getPago());
					}
					for (EgresoCaja ec : listEgresoCaja) {
						ec.setTotalFactura(redondearTotales(
								ec.getPago().subtract(egresoService.calcularTotal(ec.getCodigoDocumentoF()))));
						ec.setPago(ec.getTotalFactura());
						totalEgreso = totalEgreso.add(ec.getPago());
					}
					list.add(new CierreCajaCajero(c.getApellido() + " " + c.getNombre(), listIngresoCaja,
							listEgresoCaja, redondearTotales(totalIngreso.subtract(totalEgreso))));
				}
			}
		else {
			List<IngresoCaja> listIngresoCaja = egresoService.reporteIngresoCaja(cajero, fechaCierreCaja);
			List<EgresoCaja> listEgresoCaja = notaCreditoService.reporteEgresoCaja(cajero, fechaCierreCaja);
			if (!listIngresoCaja.isEmpty() || !listEgresoCaja.isEmpty()) {
				BigDecimal totalIngreso = newBigDecimal();
				BigDecimal totalEgreso = newBigDecimal();
				for (IngresoCaja ic : listIngresoCaja) {
					ic.setTotalFactura(egresoService.calcularTotal(ic.getCodigoDocumento()));
					totalIngreso = totalIngreso.add(ic.getPago());
				}
				for (EgresoCaja ec : listEgresoCaja) {
					ec.setTotalFactura(egresoService.calcularTotal(ec.getCodigoDocumentoF()));
					totalEgreso = totalEgreso.add(ec.getPago());
				}
				Persona p = empleadoService.obtenerPorEmpleadoCargoId(cajero).getEmpleado().getPersona();
				list.add(new CierreCajaCajero(p.getApellido() + " " + p.getNombre(), listIngresoCaja, listEgresoCaja,
						redondearTotales(totalIngreso.subtract(totalEgreso))));
			}
		}
		Map<String, Object> parametro = new HashMap<String, Object>();
		parametro.put("fechaCierre", fechaCierreCaja);
		reporteService.generarReportePDF(list, parametro, "CierreCajaCajero");
	}

	public boolean reporteCotizacion(Integer cotizacionId) {
		List<CotizacionImprimir> list = new ArrayList<CotizacionImprimir>();
		boolean bn = cotizacionService.imprimirCotizacion(cotizacionId, list);
		if (bn) {
			Cotizacion c = list.get(0).getEgreso();
			Map<String, Object> parametro = new HashMap<String, Object>();
			parametro.put("numCotizacion", c.getEstablecimiento() + "-" + c.getPuntoEmision() + "-" + c.getSecuencia());
			reporteService.generarReportePDF(list, parametro, "Cotizacion");
		}
		return bn;
	}

	@PreAuthorize("hasAnyAuthority('ADMI', 'CAJA')")
	public void reporteEntregaProducto(ActionEvent actionEvent) {
		if (listaFacturaSeleccionado.isEmpty())
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "ESCOJA UNA FACTURA PARA GENERAR EL REPORTE");
		else {
			List<EntregaProductoReporte> list = egresoService.reporteEntregaProducto(listaFacturaSeleccionado);

			for (EntregaProductoReporte epr : list) {
				List<ProductoUnidad> listaUnidades = productoService.obtenerUnidadesPorProductoId(epr.getEan())
						.getProductoUnidads();

				epr.setCantidadString("(" + epr.getCantidad() + listaUnidades.get(0).getUnidad().getAbreviatura() + ") "
						+ productoService.convertirUnidadString((int) epr.getCantidad(), listaUnidades));
			}

			String ciudades = "";
			for (int i = 0; i < ciudadVector.length; i++) {
				ciudades += ciudadService.obtenerPorCiudadId(ciudadVector[i]).getNombre().charAt(0) + "|";
			}
			Map<String, Object> parametro = new HashMap<String, Object>();
			parametro.put("fechInicio", fechaInicio);
			parametro.put("fechFin", fechaFin);
			parametro.put("CIUDAD", ciudades);
			reporteService.generarReportePDF(list, parametro, "EntregaProducto");
		}
	}

	public boolean reporteFactura(Integer facturaId) {
		List<FacturaImprimir> list = new ArrayList<FacturaImprimir>();
		boolean bn = egresoService.imprimirFactura(facturaId, list);
		if (bn) {
			if (parametro.getRuc().compareToIgnoreCase("0704812130001") == 0) {
				reporteService.generarReportePDF(list, new HashMap<String, Object>(), "Factura");
			} else {
				List<ListFacturaImprimir> list1 = new ArrayList<ListFacturaImprimir>();
				list1.add(new ListFacturaImprimir(list));
				reporteService.generarReportePDF(list1, new HashMap<String, Object>(), "FacturaC");
			}
		}
		return bn;
	}

	@PreAuthorize("hasAuthority('ADMI')")
	public void reporteGanancia(ActionEvent actionEvent) {
		List<GananciaFacturaReporte> list = egresoService.obtenerGananciaPorFechas(fechaInicio, fechaFin,
				gananciaBrutaNeta);
		if (list == null || list.isEmpty()) {
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "NO HAY DATOS QUE MOSTRAR");
		} else {
			Map<String, Object> parametro = new HashMap<String, Object>();
			parametro.put("fechaInicio", fechaInicio);
			parametro.put("fechaFin", fechaFin);
			reporteService.generarReportePDF(list, parametro, "Ganancia");
		}
		fechaInicio = null;
		fechaFin = null;
		gananciaBrutaNeta = false;
	}

	@PreAuthorize("hasAuthority('ADMI')")
	public void reporteVolumenVentaClienteProveedor(ActionEvent actionEvent) {
		if (proveedorId == 0) {
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "ESCOJA UN PROVEEDOR");
		} else if (ciudadId == 0) {
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "ESCOJA UNA CIUDAD");
		} else if (fechaInicioVVPPC.after(fechaFinVVPPC)) {
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "LA FECHA INICIO NO PUEDE ESTAR DESPUES DE LA FECHA FIN");
		} else {
			if (clienteId == 0) {
				List<ReporteDialtor> list = new ArrayList<ReporteDialtor>();
				List<VolumenVentaProductoCliente> listadoVVPC = new ArrayList<>();
				List<RefrigeradorCliente> listadoRC = new ArrayList<RefrigeradorCliente>();
				for (Cliente cliente : clienteService.obtenerPorProveedorPorCiudad(proveedorId, ciudadId,
						fechaInicioVVPPC, fechaFinVVPPC)) {
					for (Producto producto : productoService.obtenerPorProveedor(proveedorId)) {
						listadoVVPC.add(new VolumenVentaProductoCliente(producto.getNombre(),
								cliente.getPersona().getApellido() + " " + cliente.getPersona().getNombre(),
								egresoService.obtenerCantidadVendidaPorProveedorCliente(cliente.getId(),
										producto.getId(), fechaInicioVVPPC, fechaFinVVPPC).intValue()));
					}
				}
				listadoRC.add(new RefrigeradorCliente("prueba", "prueba", "prueba"));
				list.add(new ReporteDialtor(listadoVVPC, listadoRC));
				Map<String, Object> parametro = new HashMap<String, Object>();
				reporteService.generarReporteXLS(list, parametro, "ReporteCoaltor");
			} else {
				List<VolumenVentaProductoCliente> list = new ArrayList<>();
				Cliente cliente = clienteService.obtenerPorClienteId(clienteId).getCliente();

				for (Producto producto : productoService.obtenerPorProveedor(proveedorId)) {
					list.add(new VolumenVentaProductoCliente(producto.getNombre(),
							cliente.getPersona().getApellido() + " " + cliente.getPersona().getNombre(),
							egresoService.obtenerCantidadVendidaPorProveedorCliente(cliente.getId(), producto.getId(),
									fechaInicioVVPPC, fechaFinVVPPC).intValue()));
				}
				Map<String, Object> parametro = new HashMap<String, Object>();
				reporteService.generarReporteXLS(list, parametro, "ReporteCoaltorSimple");
			}
		}
	}

	@PreAuthorize("hasAnyAuthority('ADMI', 'CONT')")
	public void reporteVolumenVentaConsumidorFinal() {
		if (fechaInicio == null || fechaFin == null) {
			presentaMensaje(FacesMessage.SEVERITY_WARN, "DEBE INGRESAR LA FECHA DE INICIO Y FIN");
		} else if (fechaInicio.compareTo(fechaFin) > 0)
			presentaMensaje(FacesMessage.SEVERITY_WARN, "DEBE INGRESAR LA FECHA DE INICIO Y FIN");
		else {
			BigDecimal iva = null;
			boolean bn = true;
			if (fechaInicio.compareTo(fechaFormatoDate("31/05/2016")) <= 0
					&& fechaFin.compareTo(fechaFormatoDate("31/05/2016")) <= 0) {
				iva = newBigDecimal(12);
				bn = false;
			} else if (fechaInicio.compareTo(fechaFormatoDate("01/06/2016")) >= 0
					&& fechaFin.compareTo(fechaFormatoDate("01/06/2016")) >= 0) {
				iva = newBigDecimal(14);
				bn = false;
			}
			List<VolumenVentaFactura> list = volumenVentaFacturaService
					.obtenerVolumenventaFacturaPorConsumidorFinal(consumidorFinal, fechaInicio, fechaFin);
			for (VolumenVentaFactura vvf : list) {
				if (iva == null)
					iva = tarifaService.obtenerPorFechaImpuesto(vvf.getFecha(), Impuesto.IV).getPorcentaje();
				vvf.setIva(redondearTotales(iva(vvf.getVenta12(), iva)));
				if (bn == true)
					iva = null;
			}
			Map<String, Object> parametro = new HashMap<String, Object>();
			parametro.put("fechInicio", fechaInicio);
			parametro.put("fechFin", fechaFin);
			parametro.put("VENDEDOR", (consumidorFinal == 0) ? "CONSUMIDOR FINAL"
					: (consumidorFinal == 1) ? "SIN CONSUMIDOR FINAL" : "TODOS");

			reporteService.generarReportePDF(list, parametro, "VolumenVentaFactura");
		}
	}

	@PreAuthorize("hasAuthority('ADMI')")
	public void reporteVolumenVentaFactura(ActionEvent actionEvent) {
		BigDecimal iva = null;
		boolean bn = true;
		if (fechaInicio.compareTo(fechaFormatoDate("31/05/2016")) <= 0
				&& fechaFin.compareTo(fechaFormatoDate("31/05/2016")) <= 0) {
			iva = newBigDecimal(12);
			bn = false;
		} else if (fechaInicio.compareTo(fechaFormatoDate("01/06/2016")) >= 0
				&& fechaFin.compareTo(fechaFormatoDate("01/06/2016")) >= 0) {
			iva = newBigDecimal(14);
			bn = false;
		}
		if (vendedorId == 0) {
			List<VolumenVentaFacturaVendedor> listaVvfv = new ArrayList<VolumenVentaFacturaVendedor>();
			for (PersonaCedulaNombre pcn : listaVendedores) {
				List<VolumenVentaFactura> list = volumenVentaFacturaService
						.obtenerPorFechasAndVendedorAndEstadoDocumento(pcn.getId(), estadoDocumento, fechaInicio,
								fechaFin, ciudadIdVVF);
				BigDecimal total = newBigDecimal();
				for (VolumenVentaFactura vvf : list) {
					if (iva == null)
						iva = tarifaService.obtenerPorFechaImpuesto(vvf.getFecha(), Impuesto.IV).getPorcentaje();
					vvf.setIva(redondearTotales(iva(vvf.getVenta12(), iva)));
					vvf.setTotal(vvf.getVenta0().add(vvf.getVenta12()).add(vvf.getIva()));
					total = total.add(vvf.getTotal());
					if (bn == true)
						iva = null;
				}
				if (!list.isEmpty())
					listaVvfv.add(
							new VolumenVentaFacturaVendedor(pcn.getApellido() + " " + pcn.getNombre(), total, list));
			}
			Map<String, Object> parametro = new HashMap<String, Object>();
			parametro.put("fechInicio", fechaInicio);
			parametro.put("fechFin", fechaFin);
			reporteService.generarReportePDF(listaVvfv, parametro, "VolumenVentaFacturaVendedor");
		} else {
			List<VolumenVentaFactura> list = volumenVentaFacturaService.obtenerPorFechasAndVendedorAndEstadoDocumento(
					vendedorId, estadoDocumento, fechaInicio, fechaFin, ciudadIdVVF);
			for (VolumenVentaFactura vvf : list) {
				if (iva == null)
					iva = tarifaService.obtenerPorFechaImpuesto(vvf.getFecha(), Impuesto.IV).getPorcentaje();
				vvf.setIva(redondearTotales(iva(vvf.getVenta12(), iva)));
				if (bn == true)
					iva = null;
			}

			Persona p = empleadoService.obtenerPorEmpleadoCargoId(vendedorId).getEmpleado().getPersona();

			Map<String, Object> parametro = new HashMap<String, Object>();
			parametro.put("fechInicio", fechaInicio);
			parametro.put("fechFin", fechaFin);
			parametro.put("VENDEDOR", p.getApellido() + " " + p.getNombre());
			reporteService.generarReportePDF(list, parametro, "VolumenVentaFactura");
		}
		fechaInicio = null;
		fechaFin = null;
		vendedorId = 0;
		estadoDocumento = 0;
	}

	public void reporteVolumenVentaFacturaProveedor(ActionEvent actionEvent) {
		if (vendedorId == 0) {
			List<VolumenVentaFacturaVendedor> listaVvfv = new ArrayList<VolumenVentaFacturaVendedor>();
			for (PersonaCedulaNombre pcn : listaVendedores) {
				List<VolumenVentaFactura> list = volumenVentaFacturaService
						.obtenerPorFechasAndVendedorAndProveedorAndEstadoDocumento(pcn.getId(), estadoDocumento,
								fechaInicio, fechaFin, proveedorId);
				BigDecimal total = newBigDecimal();
				for (VolumenVentaFactura vvf : list)
					total = total.add(vvf.getTotal());
				if (!list.isEmpty())
					listaVvfv.add(
							new VolumenVentaFacturaVendedor(pcn.getApellido() + " " + pcn.getNombre(), total, list));
			}
			Persona p = proveedorService.obtenerPorId(proveedorId);
			Map<String, Object> parametro = new HashMap<String, Object>();
			parametro.put("fechInicio", fechaInicio);
			parametro.put("fechFin", fechaFin);
			parametro.put("proveedor", p.getProveedor().getNombreComercial());
			reporteService.generarReportePDF(listaVvfv, parametro, "VolumenVentaFacturaVendedor");
		} else {
			List<VolumenVentaFactura> list = volumenVentaFacturaService
					.obtenerPorFechasAndVendedorAndProveedorAndEstadoDocumento(vendedorId, estadoDocumento, fechaInicio,
							fechaFin, proveedorId);

			Persona p = empleadoService.obtenerPorEmpleadoCargoId(vendedorId).getEmpleado().getPersona();
			Persona pro = proveedorService.obtenerPorId(proveedorId);
			Map<String, Object> parametro = new HashMap<String, Object>();
			parametro.put("fechInicio", fechaInicio);
			parametro.put("fechFin", fechaFin);
			parametro.put("VENDEDOR", p.getApellido() + " " + p.getNombre());
			parametro.put("proveedor", pro.getProveedor().getNombreComercial());
			reporteService.generarReportePDF(list, parametro, "VolumenVentaFactura");
		}
		fechaInicio = null;
		fechaFin = null;
		vendedorId = 0;
		proveedorId = 0;
		estadoDocumento = 0;
	}

	@PreAuthorize("hasAuthority('ADMI')")
	public void reporteVolumenVentaProducto(ActionEvent actionEvent) {
		List<VolumenVentaProducto> list = volumenVentaProductoService.obtenerVolumenVentaProducto(fechaInicio, fechaFin,
				ordenarId, estado);
		if (!list.isEmpty()) {
			Map<String, Object> parametro = new HashMap<String, Object>();
			parametro.put("fechInicio", fechaInicio);
			parametro.put("fechFin", fechaFin);
			parametro.put("estado", estado == null ? "TODOS" : estado.getNombre());
			fechaInicio = null;
			fechaFin = null;
			reporteService.generarReportePDF(list, parametro, "VolumenVentaProducto");
		}
	}

	@PreAuthorize("hasAuthority('ADMI')")
	public void reporteVolumenVentaProductoVendedor(ActionEvent actionEvent) {
		List<VolumenVentaProductoVendedor> list = new ArrayList<VolumenVentaProductoVendedor>();
		if (vendedorId == 0)
			for (PersonaCedulaNombre pcn : listaVendedores) {
				List<VolumenVentaProducto> list1 = volumenVentaProductoService
						.obtenerVolumenVentaProductoPorVendedor(fechaInicio, fechaFin, proveedorId, pcn.getId());
				if (!list1.isEmpty())
					list.add(new VolumenVentaProductoVendedor(pcn.getApellido() + " " + pcn.getNombre(), list1));
			}
		else {
			List<VolumenVentaProducto> list1 = volumenVentaProductoService
					.obtenerVolumenVentaProductoPorVendedor(fechaInicio, fechaFin, proveedorId, vendedorId);
			if (!list1.isEmpty()) {
				Persona p = empleadoService.obtenerPorEmpleadoCargoId(vendedorId).getEmpleado().getPersona();
				list.add(new VolumenVentaProductoVendedor(p.getApellido() + " " + p.getNombre(), list1));
			}
		}
		if (!list.isEmpty()) {
			Map<String, Object> parametro = new HashMap<String, Object>();
			parametro.put("fechaInicio", fechaInicio);
			parametro.put("fechaFin", fechaFin);
			if (proveedorId != 0) {
				Persona p = proveedorService.obtenerPorId(proveedorId);
				parametro.put("proveedor", p.getProveedor().getNombreComercial());
			} else
				parametro.put("proveedor", "TODOS");
			reporteService.generarReportePDF(list, parametro, "VolumenVentaProductoVendedor");
			fechaInicio = null;
			fechaFin = null;
			vendedorId = 0;
			proveedorId = 0;
		}
	}

	public int getCajero() {
		return cajero;
	}

	public Ciudad getCiudad() {
		return ciudad;
	}

	public int getCiudadId() {
		return ciudadId;
	}

	public int getCiudadIdVVF() {
		return ciudadIdVVF;
	}

	public int[] getCiudadVector() {
		return ciudadVector;
	}

	public int getClienteId() {
		return clienteId;
	}

	public int getConsumidorFinal() {
		return consumidorFinal;
	}

	public EstadoProductoVenta getEstado() {
		return estado;
	}

	public int getEstadoDocumento() {
		return estadoDocumento;
	}

	public Date getFechaCierreCaja() {
		return fechaCierreCaja;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public Date getFechaFinVVPPC() {
		return fechaFinVVPPC;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public Date getFechaInicioVVPPC() {
		return fechaInicioVVPPC;
	}

	public List<PersonaCedulaNombre> getListaCajeros() {
		return listaCajeros;
	}

	public List<Ciudad> getListaCiudades() {
		return listaCiudades;
	}

	public List<Ciudad> getListaCiudadesVVF() {
		return listaCiudadesVVF;
	}

	public List<Ciudad> getListaCiudadesVVPPC() {
		return listaCiudadesVVPPC;
	}

	public List<Cliente> getListaClientesVVPPC() {
		return listaClientesVVPPC;
	}

	public List<Factura> getListaFacturas() {
		return listaFacturas;
	}

	public List<Factura> getListaFacturaSeleccionado() {
		return listaFacturaSeleccionado;
	}

	public List<Factura> getListaFacturasImprimir() {
		return listaFacturasImprimir;
	}

	public List<Persona> getListaProveedores() {
		return listaProveedores;
	}

	public Provincia[] getListaProvincias() {
		return Provincia.values();
	}

	public List<PersonaCedulaNombre> getListaVendedores() {
		return listaVendedores;
	}

	public EstadoProductoVenta[] getListEstadoProductoFacturas() {
		return EstadoProductoVenta.values();
	}

	public Local getLocal() {
		return local;
	}

	public int getOrdenarId() {
		return ordenarId;
	}

	public int getProveedorId() {
		return proveedorId;
	}

	public Provincia getProvincia() {
		return provincia;
	}

	public int getSecuenciaFin() {
		return secuenciaFin;
	}

	public int getSecuenciaInicio() {
		return secuenciaInicio;
	}

	public int getVendedorId() {
		return vendedorId;
	}

	public void setCajero(int cajero) {
		this.cajero = cajero;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}

	public void setCiudadId(int ciudadId) {
		this.ciudadId = ciudadId;
	}

	public void setCiudadIdVVF(int ciudadIdVVF) {
		this.ciudadIdVVF = ciudadIdVVF;
	}

	public void setCiudadVector(int[] ciudadVector) {
		this.ciudadVector = ciudadVector;
	}

	public void setClienteId(int clienteId) {
		this.clienteId = clienteId;
	}

	public void setConsumidorFinal(int consumidorFinal) {
		this.consumidorFinal = consumidorFinal;
	}

	public void setDisabledCajero(boolean disabledCajero) {
		this.disabledCajero = disabledCajero;
	}

	public void setEstado(EstadoProductoVenta estado) {
		this.estado = estado;
	}

	public void setEstadoDocumento(int estadoDocumento) {
		this.estadoDocumento = estadoDocumento;
	}

	public void setFechaCierreCaja(Date fechaCierreCaja) {
		this.fechaCierreCaja = fechaCierreCaja;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public void setFechaFinVVPPC(Date fechaFinVVPPC) {
		this.fechaFinVVPPC = fechaFinVVPPC;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public void setFechaInicioVVPPC(Date fechaInicioVVPPC) {
		this.fechaInicioVVPPC = fechaInicioVVPPC;
	}

	public void setGananciaBrutaNeta(boolean gananciaBrutaNeta) {
		this.gananciaBrutaNeta = gananciaBrutaNeta;
	}

	public void setListaCajeros(List<PersonaCedulaNombre> listaCajeros) {
		this.listaCajeros = listaCajeros;
	}

	public void setListaCiudades(List<Ciudad> listaCiudades) {
		this.listaCiudades = listaCiudades;
	}

	public void setListaCiudadesVVF(List<Ciudad> listaCiudadesVVF) {
		this.listaCiudadesVVF = listaCiudadesVVF;
	}

	public void setListaCiudadesVVPPC(List<Ciudad> listaCiudadesVVPPC) {
		this.listaCiudadesVVPPC = listaCiudadesVVPPC;
	}

	public void setListaClientesVVPPC(List<Cliente> listaClientesVVPPC) {
		this.listaClientesVVPPC = listaClientesVVPPC;
	}

	public void setListaFacturas(List<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	public void setListaFacturaSeleccionado(List<Factura> listaFacturaSeleccionado) {
		this.listaFacturaSeleccionado = listaFacturaSeleccionado;
	}

	public void setListaFacturasImprimir(List<Factura> listaFacturasImprimir) {
		this.listaFacturasImprimir = listaFacturasImprimir;
	}

	public void setListaProveedores(List<Persona> listaProveedores) {
		this.listaProveedores = listaProveedores;
	}

	public void setListaVendedores(List<PersonaCedulaNombre> listaVendedores) {
		this.listaVendedores = listaVendedores;
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	public void setOrdenarId(int ordenarId) {
		this.ordenarId = ordenarId;
	}

	public void setProveedorId(int proveedorId) {
		this.proveedorId = proveedorId;
	}

	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}

	public void setSecuenciaFin(int secuenciaFin) {
		this.secuenciaFin = secuenciaFin;
	}

	public void setSecuenciaInicio(int secuenciaInicio) {
		this.secuenciaInicio = secuenciaInicio;
	}

	public void setVendedorId(int vendedorId) {
		this.vendedorId = vendedorId;
	}

	public boolean isDisabledCajero() {
		return disabledCajero;
	}

	public boolean isGananciaBrutaNeta() {
		return gananciaBrutaNeta;
	}

	// public void reporteGiaRemision(Guiaremision guiaRemision) {
	// List<Guiaremision> list = new ArrayList<Guiaremision>();
	// list.add(guiaRemision);
	// Map<String, Object> parametro = new HashMap<String, Object>();
	// reporteService.generarReportePDF(list, parametro, "GuiaRemision");
	// }

}