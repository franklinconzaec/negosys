package ec.com.redepronik.negosys.invfac.entity;

public enum SustentoTributario {

	S00("00", "NO APLICA", new TipoComprobante[] { TipoComprobante.C01, TipoComprobante.C02, TipoComprobante.C04,
			TipoComprobante.C05, TipoComprobante.C42 }), S01("01",
					"CRÉDITO TRIBUTARIO PARA DECLARACIÓN DE IVA (SERVICIOS Y BIENES DISTINTOS DE INVENTARIOS Y ACTIVOS FIJOS)",
					new TipoComprobante[] { TipoComprobante.C01, TipoComprobante.C04, TipoComprobante.C05,
							TipoComprobante.C11, TipoComprobante.C12, TipoComprobante.C21, TipoComprobante.C41,
							TipoComprobante.C43, TipoComprobante.C47, TipoComprobante.C48 }), S02("02",
									"COSTO O GASTO PARA DECLARACIÓN DE IR (SERVICIOS Y BIENES DISTINTOS DE INVENTARIOS Y ACTIVOS FIJOS)",
									new TipoComprobante[] { TipoComprobante.C01, TipoComprobante.C02,
											TipoComprobante.C04, TipoComprobante.C05, TipoComprobante.C11,
											TipoComprobante.C12, TipoComprobante.C15, TipoComprobante.C20,
											TipoComprobante.C21, TipoComprobante.C41, TipoComprobante.C43,
											TipoComprobante.C47, TipoComprobante.C48 }), S03("03",
													"ACTIVO FIJO - CRÉDITO TRIBUTARIO PARA DECLARACIÓN DE IVA",
													new TipoComprobante[] { TipoComprobante.C01, TipoComprobante.C03,
															TipoComprobante.C04, TipoComprobante.C05,
															TipoComprobante.C41, TipoComprobante.C47,
															TipoComprobante.C48 }), S04("04",
																	"ACTIVO FIJO - COSTO O GASTO PARA DECLARACIÓN DE IR",
																	new TipoComprobante[] { TipoComprobante.C01,
																			TipoComprobante.C02, TipoComprobante.C03,
																			TipoComprobante.C04, TipoComprobante.C05,
																			TipoComprobante.C15, TipoComprobante.C41,
																			TipoComprobante.C47,
																			TipoComprobante.C48 }), S05("05",
																					"LIQUIDACIÓN GASTOS DE VIAJE, HOSPEDAJE Y ALIMENTACIÓN GASTOS IR (A NOMBRE DE EMPLEADOS Y NO DE LA EMPRESA)",
																					new TipoComprobante[] {
																							TipoComprobante.C01,
																							TipoComprobante.C02,
																							TipoComprobante.C03,
																							TipoComprobante.C04,
																							TipoComprobante.C05,
																							TipoComprobante.C11,
																							TipoComprobante.C15,
																							TipoComprobante.C41 }), S06(
																									"06",
																									"INVENTARIO - CRÉDITO TRIBUTARIO PARA DECLARACIÓN DE IVA",
																									new TipoComprobante[] {
																											TipoComprobante.C01,
																											TipoComprobante.C03,
																											TipoComprobante.C04,
																											TipoComprobante.C05,
																											TipoComprobante.C41,
																											TipoComprobante.C43,
																											TipoComprobante.C47,
																											TipoComprobante.C48 }), S07(
																													"07",
																													"INVENTARIO - COSTO O GASTO PARA DECLARACIÓN DE IR",
																													new TipoComprobante[] {
																															TipoComprobante.C01,
																															TipoComprobante.C02,
																															TipoComprobante.C03,
																															TipoComprobante.C04,
																															TipoComprobante.C05,
																															TipoComprobante.C15,
																															TipoComprobante.C41,
																															TipoComprobante.C43,
																															TipoComprobante.C47,
																															TipoComprobante.C48 }), S08(
																																	"08",
																																	"VALOR PAGADO PARA SOLICITAR REEMBOLSO DE GASTO (INTERMEDIARIO)",
																																	new TipoComprobante[] {
																																			TipoComprobante.C01,
																																			TipoComprobante.C02,
																																			TipoComprobante.C03,
																																			TipoComprobante.C04,
																																			TipoComprobante.C05,
																																			TipoComprobante.C21 }), S09(
																																					"09",
																																					"REEMBOLSO POR SINIESTROS",
																																					new TipoComprobante[] {
																																							TipoComprobante.C04,
																																							TipoComprobante.C05,
																																							TipoComprobante.C45 }), S10(
																																									"10",
																																									"DISTRIBUCIÓN DE DIVIDENDOS, BENEFICIOS O UTILIDADES",
																																									new TipoComprobante[] {
																																											TipoComprobante.C19 });

	private final String id;
	private final String nombre;
	private final TipoComprobante[] tipoComprobante;

	private SustentoTributario(String id, String nombre, TipoComprobante[] tipoComprobante) {
		this.id = id;
		this.nombre = nombre;
		this.tipoComprobante = tipoComprobante;
	}

	public String getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public TipoComprobante[] getTipoComprobante() {
		return tipoComprobante;
	}

}