package ec.com.redepronik.negosys.invfac.service;

import org.springframework.transaction.annotation.Transactional;

import ec.com.redepronik.negosys.invfac.entityAux.Anio;
import ec.com.redepronik.negosys.invfac.entityAux.Mes;
import ec.com.redepronik.negosys.utils.anexo.Anexo;

public interface AnexoService {

	@Transactional
	public Anexo generarAnexo(Anio anio, Mes mes);

}