package ec.com.redepronik.negosys.invfac.service;

import static ec.com.redepronik.negosys.utils.UtilsAplicacion.presentaMensaje;
import static ec.com.redepronik.negosys.utils.UtilsDate.dateCompleto;
import static ec.com.redepronik.negosys.utils.UtilsDate.timestamp;
import static ec.com.redepronik.negosys.utils.UtilsMath.multiplicarDivide;
import static ec.com.redepronik.negosys.utils.UtilsMath.newBigDecimal;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.application.FacesMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import ec.com.redepronik.negosys.invfac.dao.RetencionDao;
import ec.com.redepronik.negosys.invfac.entity.DetalleRetencion;
import ec.com.redepronik.negosys.invfac.entity.Retencion;
import ec.com.redepronik.negosys.rrhh.service.EmpleadoService;

@Service
public class RetencionServiceImpl implements RetencionService {

	@Autowired
	private RetencionDao retencionDao;

	@Autowired
	private EmpleadoService empleadoService;

	public void actualizar(Retencion retencion) {
		retencionDao.actualizar(retencion);
	}

	public Retencion insertar(Retencion retencion) {
		retencion.setActivo(true);
		String cedula = SecurityContextHolder.getContext().getAuthentication().getName();
		retencion.setCajero(empleadoService.obtenerEmpleadoCargoPorCedulaAndCargo(cedula, 4));
		retencion.setPuntoEmision("0");
		retencion.setSecuencia("0");
		retencion.setFechaEmision(timestamp());
		retencionDao.insertar(retencion);
		presentaMensaje(FacesMessage.SEVERITY_INFO, "INSERTO LA RETENCION # " + retencion.getId());
		return retencion;
	}

	public List<Retencion> obtener() {
		return retencionDao.obtenerPorHql("select distinct r from Retencion r where r.autorizacion is null",
				new Object[] {});
	}

	public Retencion obtenerPorId(Integer id) {
		List<Retencion> list = retencionDao.obtenerPorHql("select distinct r from Retencion r "
				+ "inner join fetch r.detallesRetenciones dr " + "where r.id=?1 order by r.fechaEmision",
				new Object[] { id });
		if (list == null || list.isEmpty())
			return null;
		else
			return list.get(0);
	}

	public Retencion obtenerPorNumeroRetencion(String establecimiento, String ptoEmision, String secuencia) {
		List<Retencion> list = retencionDao.obtenerPorHql(
				"select distinct r from Retencion r " + "inner join fetch r.detallesRetenciones dr "
						+ "where r.establecimiento=?1 and r.puntoEmision=?2 and "
						+ "r.secuencia=?3 order by r.fechaEmision",
				new Object[] { establecimiento, ptoEmision, secuencia });
		if (list == null || list.isEmpty())
			return null;
		else
			return list.get(0);
	}

	public HashMap<String, Object> imprimirRetencion(int retencionId, List<Retencion> list) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (retencionId == 0)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "NO HAY DATOS PARA IMPRIMIR");
		else {
			Retencion retencion = obtenerPorId(retencionId);
			if (retencion == null) {
				presentaMensaje(FacesMessage.SEVERITY_ERROR, "NO SE ENCONTRO LA RETENCION");
			} else if (retencion.getId() == null || retencion.getId() == 0)
				presentaMensaje(FacesMessage.SEVERITY_ERROR, "NO SE ENCONTRO LA RETENCION");
			else {
				BigDecimal totalRetenido = newBigDecimal();
				for (DetalleRetencion dr : retencion.getDetallesRetenciones())
					totalRetenido = totalRetenido
							.add(multiplicarDivide(dr.getBaseImponible(), dr.getPorcentajeRetencion()));
				list.add(retencion);
				map.put("totalRetenido", totalRetenido);
				map.put("bn", true);
				return map;
			}
		}
		map.put("bn", false);
		return map;
	}

	public void eliminar(Retencion retencion, String login, String pass) {
		boolean bn = empleadoService.autorizacion(login, pass);
		if (bn) {
			retencion.setActivo(false);
			actualizar(retencion);
			presentaMensaje(FacesMessage.SEVERITY_INFO,
					"ANULO CORRECTAMENTE LA RETENCION " + retencion.getEstablecimiento() + "-"
							+ retencion.getPuntoEmision() + "-" + retencion.getSecuencia());
		}
	}

	public List<Retencion> obtener(String criterioBusquedaProveedor, String criterioBusquedaNumeroComprobante,
			Date criterioBusquedaFechaIngreso, String criterioBusquedaNumeroRetencion,
			Date criterioBusquedaFechaRetencion) {
		criterioBusquedaProveedor = criterioBusquedaProveedor.toUpperCase();
		List<Retencion> list = new ArrayList<Retencion>();
		if (criterioBusquedaProveedor.compareToIgnoreCase("") == 0
				&& criterioBusquedaNumeroComprobante.compareToIgnoreCase("") == 0
				&& criterioBusquedaFechaIngreso == null && criterioBusquedaNumeroRetencion.compareToIgnoreCase("") == 0
				&& criterioBusquedaFechaRetencion == null)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "INGRESE UN CRITERIO DE BUSQUEDA");
		else if (criterioBusquedaProveedor.length() >= 1 && criterioBusquedaProveedor.length() <= 3)
			presentaMensaje(FacesMessage.SEVERITY_ERROR,
					"INGRESE MAS DE 3 CARACTERES PARA LA BÚSQUEDA POR PROVEEDORES");
		else if (criterioBusquedaNumeroComprobante.length() >= 1 && criterioBusquedaNumeroComprobante.length() <= 3)
			presentaMensaje(FacesMessage.SEVERITY_ERROR,
					"INGRESE MAS DE 3 CARACTERES PARA LA BÚSQUEDA POR NÚMERO DE COMPROBANTES");
		else if (!criterioBusquedaNumeroComprobante.matches("[0-9]*"))
			presentaMensaje(FacesMessage.SEVERITY_ERROR,
					"INGRESE SOLO NÚMEROS PARA LA BÚSQUEDA POR NÚMERO DE COMPROBANTES");
		else if (!criterioBusquedaNumeroRetencion.matches("[0-9]*"))
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "INGRESE SOLO NÚMEROS PARA LA BÚSQUEDA POR NÚMERO RETENCIÓN");
		else {
			if (criterioBusquedaProveedor.compareTo("") != 0)
				list = retencionDao
						.obtenerPorHql(
								"select distinct r from Retencion r " + "inner join fetch r.detallesRetenciones dr "
										+ "inner join r.proveedor pd inner join pd.persona p "
										+ "where (p.cedula like ?1 or p.apellido like ?1 or p.nombre like ?1) "
										+ "order by r.fechaEmision",
								new Object[] { "%" + criterioBusquedaProveedor + "%" });
			else if (criterioBusquedaNumeroComprobante.compareTo("") != 0)
				list = retencionDao.obtenerPorHql(
						"select distinct r from Retencion r " + "inner join fetch r.detallesRetenciones dr "
								+ "inner join r.proveedor pd inner join pd.persona p " + "where r.numeroCompra like ?1 "
								+ "order by r.fechaEmision",
						new Object[] { "%" + criterioBusquedaNumeroComprobante + "%" });
			else if (criterioBusquedaFechaIngreso != null)
				list = retencionDao.obtenerPorHql(
						"select distinct r from Retencion r " + "inner join fetch r.detallesRetenciones dr "
								+ "inner join r.proveedor pd inner join pd.persona p "
								+ "where r.fechaCompra>=?1 and r.fechaCompra<=?2 " + "order by r.fechaEmision",
						new Object[] { criterioBusquedaFechaIngreso, dateCompleto(criterioBusquedaFechaIngreso) });
			else if (criterioBusquedaNumeroRetencion.compareTo("") != 0)
				list = retencionDao.obtenerPorHql(
						"select distinct r from Retencion r " + "inner join fetch r.detallesRetenciones dr "
								+ "inner join r.proveedor pd inner join pd.persona p " + "where r.secuencia like ?1 "
								+ "order by r.fechaEmision",
						new Object[] { String.format("%09d", Integer.parseInt(criterioBusquedaNumeroRetencion)) });
			else if (criterioBusquedaFechaRetencion != null)
				list = retencionDao.obtenerPorHql(
						"select distinct r from Retencion r " + "inner join fetch r.detallesRetenciones dr "
								+ "inner join r.proveedor pd inner join pd.persona p "
								+ "where r.fechaEmision>=?1 and r.fechaEmision<=?2 " + "order by r.fechaEmision",
						new Object[] { criterioBusquedaFechaRetencion, dateCompleto(criterioBusquedaFechaRetencion) });
			if (list.isEmpty())
				presentaMensaje(FacesMessage.SEVERITY_INFO, "NO SE ENCONTRARON COINCIDENCIAS");
		}
		return list;
	}

	public List<Retencion> obtenerParaAnexo(Date fechaInicio, Date fechaFin) {
		List<Retencion> list = new ArrayList<Retencion>();
		if (fechaFin == null && fechaFin == null)
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "INGRESE UN CRITERIO DE BUSQUEDA");
		else
			list = retencionDao
					.obtenerPorHql("select distinct r from Retencion r " + "inner join fetch r.detallesRetenciones dr "
							+ "where r.fechaCompra>=?1 and r.fechaCompra<=?2 and r.activo=true and r.autorizacion is not null "
							+ "order by r.fechaEmision", new Object[] { fechaInicio, dateCompleto(fechaFin) });
		if (list.isEmpty())
			presentaMensaje(FacesMessage.SEVERITY_INFO, "NO SE ENCONTRARON COINCIDENCIAS");
		return list;
	}
}