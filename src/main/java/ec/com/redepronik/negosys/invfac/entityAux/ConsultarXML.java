package ec.com.redepronik.negosys.invfac.entityAux;

import static ec.com.redepronik.negosys.utils.UtilsMath.newBigDecimal;

import java.math.BigDecimal;
import java.util.Date;

import ec.com.redepronik.negosys.invfac.entity.TipoComprobante;
import ec.com.redepronik.negosys.rrhh.entity.Proveedor;

public class ConsultarXML {

	private TipoComprobante tipoComprobante;
	private String ambiente;
	private Proveedor proveedor;
	private String nombre;
	private String estado;
	private String numDocu;
	private Date fechaEmi;
	private String numAut;
	private Date fechaAut;
	private BigDecimal base0;
	private BigDecimal base12;
	private BigDecimal iva;
	private BigDecimal total;

	public ConsultarXML() {
		base0 = newBigDecimal();
		base12 = newBigDecimal();
		iva = newBigDecimal();
		total = newBigDecimal();
	}

	public ConsultarXML(TipoComprobante tipoComprobante, String ambiente, Proveedor proveedor, String nombre,
			String estado, String numDocu, Date fechaEmi, String numAut, Date fechaAut, BigDecimal base0,
			BigDecimal base12, BigDecimal iva, BigDecimal total) {
		this.tipoComprobante = tipoComprobante;
		this.ambiente = ambiente;
		this.proveedor = proveedor;
		this.nombre = nombre;
		this.estado = estado;
		this.numDocu = numDocu;
		this.fechaEmi = fechaEmi;
		this.numAut = numAut;
		this.fechaAut = fechaAut;
		this.base0 = base0;
		this.base12 = base12;
		this.iva = iva;
		this.total = total;
	}

	public TipoComprobante getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(TipoComprobante tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	public String getAmbiente() {
		return ambiente;
	}

	public void setAmbiente(String ambiente) {
		this.ambiente = ambiente;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getNumDocu() {
		return numDocu;
	}

	public void setNumDocu(String numDocu) {
		this.numDocu = numDocu;
	}

	public Date getFechaEmi() {
		return fechaEmi;
	}

	public void setFechaEmi(Date fechaEmi) {
		this.fechaEmi = fechaEmi;
	}

	public String getNumAut() {
		return numAut;
	}

	public void setNumAut(String numAut) {
		this.numAut = numAut;
	}

	public Date getFechaAut() {
		return fechaAut;
	}

	public void setFechaAut(Date fechaAut) {
		this.fechaAut = fechaAut;
	}

	public BigDecimal getBase0() {
		return base0;
	}

	public void setBase0(BigDecimal base0) {
		this.base0 = base0;
	}

	public BigDecimal getBase12() {
		return base12;
	}

	public void setBase12(BigDecimal base12) {
		this.base12 = base12;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

}
