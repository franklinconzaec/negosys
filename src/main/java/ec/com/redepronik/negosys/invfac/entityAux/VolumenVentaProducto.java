package ec.com.redepronik.negosys.invfac.entityAux;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class VolumenVentaProducto implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private Long id;
	private String nombre;
	private Long cantidad;
	private String cantidadString;
	private BigDecimal gananciaNeta;

	public VolumenVentaProducto() {
	}

	public VolumenVentaProducto(Long id, String nombre, Long cantidad) {
		this.id = id;
		this.nombre = nombre;
		this.cantidad = cantidad;
	}

	public VolumenVentaProducto(Long id, String nombre, Long cantidad, BigDecimal gananciaNeta) {
		this.id = id;
		this.nombre = nombre;
		this.cantidad = cantidad;
		this.gananciaNeta = gananciaNeta;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

	public String getCantidadString() {
		return cantidadString;
	}

	public void setCantidadString(String cantidadString) {
		this.cantidadString = cantidadString;
	}

	public BigDecimal getGananciaNeta() {
		return gananciaNeta;
	}

	public void setGananciaNeta(BigDecimal gananciaNeta) {
		this.gananciaNeta = gananciaNeta;
	}

}