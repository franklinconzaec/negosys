package ec.com.redepronik.negosys.invfac.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import ec.com.redepronik.negosys.invfac.entityAux.VentasAnexo;

public interface VentasAnexoService {

	@Transactional
	public List<VentasAnexo> obtenerParaAnexoVentas(Date fechaInicio, Date fechaFin, BigDecimal iva);

	@Transactional
	public List<VentasAnexo> obtenerParaAnexoNC(Date fechaInicio, Date fechaFin, BigDecimal iva);

	@Transactional
	public List<VentasAnexo> obtenerParaAnexoVentasEst(Date fechaInicio, Date fechaFin);
}
