package ec.com.redepronik.negosys.invfac.service;

import static ec.com.redepronik.negosys.utils.UtilsDate.fechaFormatoDate;
import static ec.com.redepronik.negosys.utils.UtilsDate.getPrimerDiaDelMes;
import static ec.com.redepronik.negosys.utils.UtilsDate.getUltimoDiaDelMes;
import static ec.com.redepronik.negosys.utils.UtilsMath.newBigDecimal;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.com.redepronik.negosys.invfac.entity.Anulado;
import ec.com.redepronik.negosys.invfac.entity.Impuesto;
import ec.com.redepronik.negosys.invfac.entity.Retencion;
import ec.com.redepronik.negosys.invfac.entityAux.Anio;
import ec.com.redepronik.negosys.invfac.entityAux.Mes;
import ec.com.redepronik.negosys.invfac.entityAux.VentasAnexo;
import ec.com.redepronik.negosys.utils.anexo.Anexo;
import ec.com.redepronik.negosys.utils.anexo.UtilsAnexo;

@Service
public class AnexoServiceImpl implements AnexoService {

	@Autowired
	private RetencionService retencionService;

	@Autowired
	private AnuladoService anuladoService;

	@Autowired
	private VentasAnexoService ventasAnexoService;

	@Autowired
	private LocalService localService;

	@Autowired
	private TarifaService tarifaService;

	public Anexo generarAnexo(Anio anio, Mes mes) {
		Date fecha = fechaFormatoDate("01/" + mes.getId() + "/" + anio.getId());
		// if (Integer.parseInt(mes.getId()) <= Integer
		// .parseInt(fechaFormatoString(new Date()).substring(3, 5))) {
		Date fechaInicio = getPrimerDiaDelMes(fecha);
		Date fechaFin = getUltimoDiaDelMes(fecha);
		System.out.println("********** " + fechaInicio);
		BigDecimal iva = tarifaService.obtenerPorFechaImpuesto(fechaInicio, Impuesto.IV).getPorcentaje();
		System.out.println("***** " + iva);
		List<Retencion> listCompras = retencionService.obtenerParaAnexo(fechaInicio, fechaFin);
		List<VentasAnexo> listVentasAnexos = ventasAnexoService.obtenerParaAnexoVentas(fechaInicio, fechaFin, iva);
		listVentasAnexos.addAll(ventasAnexoService.obtenerParaAnexoNC(fechaInicio, fechaFin, iva));
		List<VentasAnexo> listVentasEst = ventasAnexoService.obtenerParaAnexoVentasEst(fechaInicio, fechaFin);
		List<Anulado> listAnulados = anuladoService.obtenerParaAnexo(fechaInicio, fechaFin);
		BigDecimal totalVentas = newBigDecimal();

		for (VentasAnexo va : listVentasEst)
			totalVentas = totalVentas.add(va.getTotal());
		return UtilsAnexo.anexoXML(anio.getId(), mes.getId(), totalVentas, listCompras, listVentasAnexos, listVentasEst,
				listAnulados, localService.obtenerNumeroEstablecimientos(), iva);
		// }
		// return null;
	}
}