package ec.com.redepronik.negosys.invfac.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "anulados")
public class Anulado implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private TipoComprobante tipoComprobante;
	private String establecimiento;
	private String puntoEmision;
	private String secuenciaInicio;
	private String secuenciaFin;
	private String autorizacion;
	private Date fechaAnulado;

	public Anulado() {
	}

	public Anulado(Integer id, TipoComprobante tipoComprobante, String establecimiento, String puntoEmision,
			String secuenciaInicio, String secuenciaFin, String autorizacion, Date fechaAnulado) {
		this.id = id;
		this.tipoComprobante = tipoComprobante;
		this.establecimiento = establecimiento;
		this.puntoEmision = puntoEmision;
		this.secuenciaInicio = secuenciaInicio;
		this.secuenciaFin = secuenciaFin;
		this.autorizacion = autorizacion;
		this.fechaAnulado = fechaAnulado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Anulado other = (Anulado) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Id
	@SequenceGenerator(allocationSize = 1, name = "ANULADOS_ID_GENERATOR", sequenceName = "ANULADOS_ID_SEQ")
	// @GeneratedValue(strategy = GenerationType.IDENTITY)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ANULADOS_ID_GENERATOR")
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "tipocomprobante", nullable = false)
	public TipoComprobante getTipoComprobante() {
		return tipoComprobante;
	}

	@Column(length = 3, nullable = false)
	public String getEstablecimiento() {
		return establecimiento;
	}

	@Column(name = "puntoemision", length = 3, nullable = false)
	public String getPuntoEmision() {
		return puntoEmision;
	}

	@Column(length = 9, nullable = false)
	public String getSecuenciaInicio() {
		return secuenciaInicio;
	}

	@Column(length = 9, nullable = false)
	public String getSecuenciaFin() {
		return secuenciaFin;
	}

	@Column(name = "autorizacion", length = 37)
	public String getAutorizacion() {
		return autorizacion;
	}

	@Column(name = "fechaanulado", nullable = false)
	public Date getFechaAnulado() {
		return fechaAnulado;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setTipoComprobante(TipoComprobante tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	public void setEstablecimiento(String establecimiento) {
		this.establecimiento = establecimiento;
	}

	public void setPuntoEmision(String puntoEmision) {
		this.puntoEmision = puntoEmision;
	}

	public void setSecuenciaInicio(String secuenciaInicio) {
		this.secuenciaInicio = secuenciaInicio;
	}

	public void setSecuenciaFin(String secuenciaFin) {
		this.secuenciaFin = secuenciaFin;
	}

	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}

	public void setFechaAnulado(Date fechaAnulado) {
		this.fechaAnulado = fechaAnulado;
	}

}