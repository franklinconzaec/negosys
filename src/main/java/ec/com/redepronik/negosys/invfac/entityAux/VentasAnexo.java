package ec.com.redepronik.negosys.invfac.entityAux;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class VentasAnexo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String cedula;
	private String tipoComprobante;
	private long numCompro;
	private BigDecimal venta0;
	private BigDecimal venta12;
	private BigDecimal iva;
	private BigDecimal retIva;
	private BigDecimal retRenta;
	private BigDecimal total;

	public VentasAnexo() {
	}

	public VentasAnexo(String cedula, BigDecimal total) {
		this.cedula = cedula;
		this.total = total;
	}

	public VentasAnexo(String cedula, String tipoComprobante, long numCompro, BigDecimal venta0, BigDecimal venta12,
			BigDecimal iva, BigDecimal retIva, BigDecimal retRenta) {
		this.cedula = cedula;
		this.tipoComprobante = tipoComprobante;
		this.numCompro = numCompro;
		this.venta0 = venta0;
		this.venta12 = venta12;
		this.iva = iva;
		this.retIva = retIva;
		this.retRenta = retRenta;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(String tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	public long getNumCompro() {
		return numCompro;
	}

	public void setNumCompro(long numCompro) {
		this.numCompro = numCompro;
	}

	public BigDecimal getVenta0() {
		return venta0;
	}

	public void setVenta0(BigDecimal venta0) {
		this.venta0 = venta0;
	}

	public BigDecimal getVenta12() {
		return venta12;
	}

	public void setVenta12(BigDecimal venta12) {
		this.venta12 = venta12;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getRetIva() {
		return retIva;
	}

	public void setRetIva(BigDecimal retIva) {
		this.retIva = retIva;
	}

	public BigDecimal getRetRenta() {
		return retRenta;
	}

	public void setRetRenta(BigDecimal retRenta) {
		this.retRenta = retRenta;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}