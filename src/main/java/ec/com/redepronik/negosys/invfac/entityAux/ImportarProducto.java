package ec.com.redepronik.negosys.invfac.entityAux;

import java.math.BigDecimal;

public class ImportarProducto {
	private String nombre;
	private String grupo;
	private String ean;
	private String codigo1;
	private String tipo;
	private BigDecimal pc;
	private int cm;
	private boolean kardex;
	private boolean iva;
	private BigDecimal pv;
	private String unidad;

	public ImportarProducto() {
	}

	public ImportarProducto(String nombre, String grupo, String ean, String codigo1, String tipo, BigDecimal pc, int cm,
			boolean kardex, boolean iva, BigDecimal pv, String unidad) {
		this.nombre = nombre;
		this.grupo = grupo;
		this.ean = ean;
		this.codigo1 = codigo1;
		this.tipo = tipo;
		this.pc = pc;
		this.cm = cm;
		this.kardex = kardex;
		this.iva = iva;
		this.pv = pv;
		this.unidad = unidad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public String getEan() {
		return ean;
	}

	public void setEan(String ean) {
		this.ean = ean;
	}

	public String getCodigo1() {
		return codigo1;
	}

	public void setCodigo1(String codigo1) {
		this.codigo1 = codigo1;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public BigDecimal getPc() {
		return pc;
	}

	public void setPc(BigDecimal pc) {
		this.pc = pc;
	}

	public int getCm() {
		return cm;
	}

	public void setCm(int cm) {
		this.cm = cm;
	}

	public boolean isKardex() {
		return kardex;
	}

	public void setKardex(boolean kardex) {
		this.kardex = kardex;
	}

	public boolean isIva() {
		return iva;
	}

	public void setIva(boolean iva) {
		this.iva = iva;
	}

	public BigDecimal getPv() {
		return pv;
	}

	public void setPv(BigDecimal pv) {
		this.pv = pv;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

}
