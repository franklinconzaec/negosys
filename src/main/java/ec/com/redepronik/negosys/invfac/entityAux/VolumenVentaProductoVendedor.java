package ec.com.redepronik.negosys.invfac.entityAux;

import java.io.Serializable;
import java.util.List;

public class VolumenVentaProductoVendedor implements Serializable {

	private static final long serialVersionUID = 1L;

	private String vendedor;
	private List<VolumenVentaProducto> list;

	public VolumenVentaProductoVendedor() {
	}

	public VolumenVentaProductoVendedor(String vendedor, List<VolumenVentaProducto> list) {
		this.vendedor = vendedor;
		this.list = list;
	}

	public String getVendedor() {
		return vendedor;
	}

	public void setVendedor(String vendedor) {
		this.vendedor = vendedor;
	}

	public List<VolumenVentaProducto> getList() {
		return list;
	}

	public void setList(List<VolumenVentaProducto> list) {
		this.list = list;
	}

}