package ec.com.redepronik.negosys.invfac.dao;

import org.springframework.stereotype.Repository;

import ec.com.redepronik.negosys.invfac.entity.Anulado;
import ec.com.redepronik.negosys.utils.dao.GenericDaoImpl;

@Repository
public class AnuladoDaoImpl extends GenericDaoImpl<Anulado, Integer> implements AnuladoDao {

}