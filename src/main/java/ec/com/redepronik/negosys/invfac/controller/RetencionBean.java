package ec.com.redepronik.negosys.invfac.controller;

import static ec.com.redepronik.negosys.utils.UtilsAplicacion.presentaMensaje;
import static ec.com.redepronik.negosys.utils.UtilsMath.iva;
import static ec.com.redepronik.negosys.utils.UtilsMath.newBigDecimal;
import static ec.com.redepronik.negosys.utils.UtilsMath.parametro;
import static ec.com.redepronik.negosys.utils.UtilsMath.redondearTotales;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import ec.com.redepronik.negosys.invfac.entity.DetalleRetencion;
import ec.com.redepronik.negosys.invfac.entity.Impuesto;
import ec.com.redepronik.negosys.invfac.entity.ImpuestoRetencion;
import ec.com.redepronik.negosys.invfac.entity.Local;
import ec.com.redepronik.negosys.invfac.entity.Retencion;
import ec.com.redepronik.negosys.invfac.entity.SustentoTributario;
import ec.com.redepronik.negosys.invfac.entity.TarifaRetencion;
import ec.com.redepronik.negosys.invfac.entity.TipoComprobante;
import ec.com.redepronik.negosys.invfac.entityAux.ConsultarXML;
import ec.com.redepronik.negosys.invfac.service.DocumentosElectronicosService;
import ec.com.redepronik.negosys.invfac.service.LocalService;
import ec.com.redepronik.negosys.invfac.service.RetencionService;
import ec.com.redepronik.negosys.invfac.service.TarifaService;
import ec.com.redepronik.negosys.rrhh.entity.Persona;
import ec.com.redepronik.negosys.rrhh.entity.Proveedor;
import ec.com.redepronik.negosys.rrhh.service.ProveedorService;

@Controller
@Scope("session")
public class RetencionBean {

	@Autowired
	private ProveedorService proveedorService;

	@Autowired
	private RetencionService retencionService;

	@Autowired
	private TarifaService tarifaService;

	@Autowired
	private LocalService localService;

	@Autowired
	private DocumentosElectronicosService documentosElectronicosService;

	private Retencion retencion;
	private String proveedor;
	private BigDecimal valorRetenido;
	private TarifaRetencion[] listaTarifas;
	private DetalleRetencion detalleRetencion;
	private List<Local> listaLocales;
	private BigDecimal totalRetenido = newBigDecimal();
	private BigDecimal iva = redondearTotales(newBigDecimal());
	private BigDecimal total = redondearTotales(newBigDecimal());
	private boolean bnAutorizacion;
	private String claveAcceso;
	private ConsultarXML consultarXML;

	public RetencionBean() {
	}

	public boolean isBnAutorizacion() {
		return bnAutorizacion;
	}

	public void setBnAutorizacion(boolean bnAutorizacion) {
		this.bnAutorizacion = bnAutorizacion;
	}

	public BigDecimal getTotalRetenido() {
		return redondearTotales(totalRetenido);
	}

	public void setTotalRetenido(BigDecimal totalRetenido) {
		this.totalRetenido = totalRetenido;
	}

	public void cargarProveedor() {
		retencion.setProveedor(proveedorService.cargarProveedor(proveedor));
	}

	public ConsultarXML getConsultarXML() {
		return consultarXML;
	}

	public void setConsultarXML(ConsultarXML consultarXML) {
		this.consultarXML = consultarXML;
	}

	public DetalleRetencion getDetalleRetencion() {
		return detalleRetencion;
	}

	public SustentoTributario[] getListaSustento() {
		return SustentoTributario.values();
	}

	public TipoComprobante[] getListaComprobantes() {
		return TipoComprobante.values();
	}

	public ImpuestoRetencion[] getListaImpuestos() {
		return ImpuestoRetencion.values();
	}

	public TarifaRetencion[] getListaTarifas() {
		return listaTarifas;
	}

	public String getProveedor() {
		return proveedor;
	}

	public ProveedorService getProveedorService() {
		return proveedorService;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public String getClaveAcceso() {
		return claveAcceso;
	}

	public void setClaveAcceso(String claveAcceso) {
		this.claveAcceso = claveAcceso;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Retencion getRetencion() {
		return retencion;
	}

	public BigDecimal getValorRetenido() {
		return valorRetenido;
	}

	@PostConstruct
	public void init() {
		limpiarRetencion();
		listaLocales = localService.obtener(true);
		bnAutorizacion = parametro.getFacturacionElectronica();
	}

	public void limpiarRetencion() {
		retencion = new Retencion();
		retencion.setProveedor(new Proveedor());
		retencion.getProveedor().setPersona(new Persona());
		retencion.setDetallesRetenciones(new ArrayList<DetalleRetencion>());
		proveedor = "";
		detalleRetencion = new DetalleRetencion();
		totalRetenido = newBigDecimal();
		iva = redondearTotales(newBigDecimal());
		total = redondearTotales(newBigDecimal());
	}

	public void insertar() {
		if (retencion.getProveedor() == null || proveedor.compareTo("") == 0)
			presentaMensaje(FacesMessage.SEVERITY_WARN, "ESCOJA UN PROVEEDOR");
		else if (retencion.getEstablecimiento().compareTo("") == 0)
			presentaMensaje(FacesMessage.SEVERITY_WARN, "ESCOJA UN ESTABLECIMIENTO");
		else if (retencion.getTipoCompraRetencion() == null)
			presentaMensaje(FacesMessage.SEVERITY_WARN, "ESCOJA UN TIPO DE COMPROBANTE", "error", true);
		else if (retencion.getNumeroCompra().compareTo("") == 0 || retencion.getNumeroCompra().length() != 15)
			presentaMensaje(FacesMessage.SEVERITY_WARN, "INGRESE LOS 15 DIGITOS DEL COMPROBANTE", "error", true);
		else if (retencion.getAutorizacionCompra() == null || retencion.getAutorizacionCompra().compareTo("") == 0)
			presentaMensaje(FacesMessage.SEVERITY_WARN, "INGRESE EL NUMERO DE AUTORIZACION", "error", true);
		else if (retencion.getFechaCompra() == null)
			presentaMensaje(FacesMessage.SEVERITY_WARN, "INGRESE LA FECHA DEL COMPROBANTE", "error", true);
		else if (retencion.getDetallesRetenciones() == null || retencion.getDetallesRetenciones().isEmpty())
			presentaMensaje(FacesMessage.SEVERITY_WARN, "INGRESE AL MENOS UN DETALLE");
		else {
			Integer retencionId = retencionService.insertar(retencion).getId();
			if (retencionId != null) {
				// tributacionReportes.reporteRetencion(retencionId);
				documentosElectronicosService.generarRetencionXML(retencionId);
				limpiarRetencion();
			}

		}
	}

	public void consultar() {
		consultarXML = documentosElectronicosService.consultarXML(claveAcceso);
		if (consultarXML != null)
			claveAcceso = null;
	}

	public void importar() {
		if (consultarXML == null)
			presentaMensaje(FacesMessage.SEVERITY_WARN, "CONSULTE UN COMPROBANTE PRIMERO");
		else if (consultarXML.getProveedor() == null)
			presentaMensaje(FacesMessage.SEVERITY_WARN, "INGRESE EL PROVEEDOR PRIMERO");
		else {
			Persona p = consultarXML.getProveedor().getPersona();
			proveedor = p.getCedula() + " - " + p.getApellido() + " " + p.getNombre() + " - "
					+ consultarXML.getProveedor().getNombreComercial();
			retencion.setProveedor(consultarXML.getProveedor());
			retencion.setSustentoTributario(SustentoTributario.S07);
			retencion.setTipoCompraRetencion(consultarXML.getTipoComprobante());
			retencion.setNumeroCompra(consultarXML.getNumDocu());
			retencion.setAutorizacionCompra(consultarXML.getNumAut());
			retencion.setFechaCompra(consultarXML.getFechaEmi());
			retencion.setBase0(consultarXML.getBase0());
			retencion.setBase12(consultarXML.getBase12());
			iva = consultarXML.getIva();
			total = consultarXML.getTotal();
			presentaMensaje(FacesMessage.SEVERITY_INFO, "SE IMPORTO CORRECTAMENTE");
		}
		limpiarImportar();
	}

	public void limpiarImportar() {
		claveAcceso = null;
		consultarXML = null;
	}

	public void calcularTotales() {
		if (retencion.getBase0() == null)
			retencion.setBase0(newBigDecimal());
		if (retencion.getBase12() == null)
			retencion.setBase12(newBigDecimal());
		System.out.println("********** " + retencion.getFechaCompra());
		BigDecimal porIva = tarifaService.obtenerPorFechaImpuesto(retencion.getFechaCompra(), Impuesto.IV)
				.getPorcentaje();
		System.out.println("***** " + porIva);
		iva = redondearTotales(iva(retencion.getBase12(), porIva));
		total = redondearTotales(retencion.getBase0().add(retencion.getBase12()).add(iva));
	}

	public void insertarDetalle() {
		if (detalleRetencion != null) {
			if (detalleRetencion.getImpuestoRetencion() == null) {
				presentaMensaje(FacesMessage.SEVERITY_WARN, "ESCOJA UN IMPUESTO", "cerrar", false);
			} else if (detalleRetencion.getTarifa() == null) {
				presentaMensaje(FacesMessage.SEVERITY_WARN, "ESCOJA UNA TARIFA", "cerrar", false);
			} else if (detalleRetencion.getPorcentajeRetencion().compareTo(new BigDecimal("0.00")) == 0) {
				presentaMensaje(FacesMessage.SEVERITY_WARN, "INGRESE UN PORCENTAJE", "cerrar", false);
			} else if (detalleRetencion.getBaseImponible().compareTo(new BigDecimal("0.00")) == 0) {
				presentaMensaje(FacesMessage.SEVERITY_WARN, "INGRESE LA BASE IMPONIBLE", "cerrar", false);
			} else {
				retencion.addDetalleRetencion(detalleRetencion);
				presentaMensaje(FacesMessage.SEVERITY_INFO,
						"SE AGREGO CORRECTAMENTE EL COMPROBANTE " + detalleRetencion.getImpuestoRetencion().getNombre(),
						"cerrar", true);
				detalleRetencion = new DetalleRetencion();
				totalRetenido = totalRetenido.add(valorRetenido);
				System.out.println(totalRetenido);
				valorRetenido = new BigDecimal("0.00");
			}
		}
	}

	public void obtenerListaTarifas() {
		listaTarifas = TarifaRetencion.obtenerPorImpuesto(detalleRetencion.getImpuestoRetencion());
		if (retencion.getBase0() != null && retencion.getBase12() != null)
			if (detalleRetencion.getImpuestoRetencion() == ImpuestoRetencion.IV)
				detalleRetencion.setBaseImponible(iva);
			else
				detalleRetencion.setBaseImponible(retencion.getBase0().add(retencion.getBase12()));
	}

	public void obtenerPorcentaje() {
		detalleRetencion.setPorcentajeRetencion(redondearTotales(detalleRetencion.getTarifa().getPorcentaje()));
		obtenerValorRetenido();
	}

	public List<String> obtenerProveedorPorBusqueda(String criterioProveedorBusqueda) {
		List<String> lista = proveedorService.obtenerListaProveedoresAutoComplete(criterioProveedorBusqueda);
		if (lista.size() == 1) {
			proveedor = lista.get(0);
			cargarProveedor();
		}
		return lista;
	}

	public void obtenerValorRetenido() {
		valorRetenido = redondearTotales(detalleRetencion.getBaseImponible()
				.multiply(detalleRetencion.getPorcentajeRetencion()).divide(new BigDecimal(100)));
	}

	public void cambiarPorcentaje() {
		detalleRetencion.setPorcentajeRetencion(detalleRetencion.getPorcentajeRetencion());
		obtenerValorRetenido();
	}

	public void setDetalleRetencion(DetalleRetencion detalleRetencion) {
		this.detalleRetencion = detalleRetencion;
	}

	public void setListaTarifas(TarifaRetencion[] listaTarifas) {
		this.listaTarifas = listaTarifas;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public void setProveedorService(ProveedorService proveedorService) {
		this.proveedorService = proveedorService;
	}

	public void setRetencion(Retencion retencion) {
		this.retencion = retencion;
	}

	public void setValorRetenido(BigDecimal valorRetenido) {
		this.valorRetenido = valorRetenido;
	}

	public List<Local> getListaLocales() {
		return listaLocales;
	}

	public void setListaLocales(List<Local> listaLocales) {
		this.listaLocales = listaLocales;
	}

}