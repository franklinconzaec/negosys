package ec.com.redepronik.negosys.invfac.entityAux;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class GananciaFacturaReporte implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private Integer id;
	@Column(name = "codigo_documento")
	private String codigoDocumento;
	@Column(name = "fecha_factura")
	private Date fechaFactura;
	@Column(name = "fecha_pago")
	private Date fechaPago;
	@Column(name = "cliente")
	private String cliente;
	@Column(name = "precio_venta")
	private BigDecimal precioVenta;
	@Column(name = "precio_costo")
	private BigDecimal precioCosto;
	@Column(name = "descuento_producto")
	private BigDecimal descuentoProducto;
	@Column(name = "ganancia")
	private BigDecimal ganancia;

	public GananciaFacturaReporte() {

	}

	public GananciaFacturaReporte(String codigoDocumento, Date fechaFactura, Date fechaPago, BigDecimal precioVenta,
			BigDecimal precioCosto, BigDecimal descuentoProducto, BigDecimal ganancia) {
		this.codigoDocumento = codigoDocumento;
		this.fechaFactura = fechaFactura;
		this.fechaPago = fechaPago;
		this.precioVenta = precioVenta;
		this.precioCosto = precioCosto;
		this.descuentoProducto = descuentoProducto;
		this.ganancia = ganancia;
	}

	public GananciaFacturaReporte(Integer id, String codigoDocumento, Date fechaFactura, Date fechaPago, String cliente,
			BigDecimal precioVenta, BigDecimal precioCosto, BigDecimal descuentoProducto, BigDecimal ganancia) {
		this.id = id;
		this.codigoDocumento = codigoDocumento;
		this.fechaFactura = fechaFactura;
		this.fechaPago = fechaPago;
		this.cliente = cliente;
		this.precioVenta = precioVenta;
		this.precioCosto = precioCosto;
		this.descuentoProducto = descuentoProducto;
		this.ganancia = ganancia;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigoDocumento() {
		return codigoDocumento;
	}

	public void setCodigoDocumento(String codigoDocumento) {
		this.codigoDocumento = codigoDocumento;
	}

	public Date getFechaFactura() {
		return fechaFactura;
	}

	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public BigDecimal getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(BigDecimal precioVenta) {
		this.precioVenta = precioVenta;
	}

	public BigDecimal getPrecioCosto() {
		return precioCosto;
	}

	public void setPrecioCosto(BigDecimal precioCosto) {
		this.precioCosto = precioCosto;
	}

	public BigDecimal getDescuentoProducto() {
		return descuentoProducto;
	}

	public void setDescuentoProducto(BigDecimal descuentoProducto) {
		this.descuentoProducto = descuentoProducto;
	}

	public BigDecimal getGanancia() {
		return ganancia;
	}

	public void setGanancia(BigDecimal ganancia) {
		this.ganancia = ganancia;
	}

}
