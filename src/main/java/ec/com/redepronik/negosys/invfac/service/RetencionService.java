package ec.com.redepronik.negosys.invfac.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import ec.com.redepronik.negosys.invfac.entity.Retencion;

public interface RetencionService {
	@Transactional
	public void actualizar(Retencion retencion);

	@Transactional
	public Retencion insertar(Retencion retencion);

	@Transactional
	public List<Retencion> obtener();

	@Transactional
	public Retencion obtenerPorNumeroRetencion(String establecimiento, String ptoEmision, String secuencia);

	@Transactional
	public Retencion obtenerPorId(Integer id);

	@Transactional
	public HashMap<String, Object> imprimirRetencion(int retencionId, List<Retencion> list);

	@Transactional
	public void eliminar(Retencion retencion, String login, String pass);

	@Transactional
	public List<Retencion> obtener(String criterioBusquedaProveedor, String criterioBusquedaNumeroComprobante,
			Date criterioBusquedaFechaIngreso, String criterioBusquedaNumeroRetencion,
			Date criterioBusquedaFechaRetencion);

	@Transactional
	public List<Retencion> obtenerParaAnexo(Date fechaInicio, Date fechaFin);
}