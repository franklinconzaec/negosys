package ec.com.redepronik.negosys.invfac.service;

import static ec.com.redepronik.negosys.utils.UtilsDate.dateCompleto;
import static ec.com.redepronik.negosys.utils.UtilsDate.dateFin;
import static ec.com.redepronik.negosys.utils.UtilsDate.dateInicio;
import static ec.com.redepronik.negosys.utils.UtilsMath.divide100;
import static ec.com.redepronik.negosys.utils.UtilsMath.redondearTotales;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.com.redepronik.negosys.invfac.dao.VentasAnexoDao;
import ec.com.redepronik.negosys.invfac.entityAux.VentasAnexo;

@Service
public class VentasAnexoServiceImpl implements VentasAnexoService {

	@Autowired
	private VentasAnexoDao ventasAnexoDao;

	public List<VentasAnexo> obtenerParaAnexoVentas(Date fechaInicio, Date fechaFin, BigDecimal iva) {
		return ventasAnexoDao.obtenerPorSql("select distinct p.cedula as cedula, '18' as tipoComprobante, "
				+ "(select count(f1.\"clienteFactura\") from negosys.facturas f1 "
				+ "where f1.\"clienteFactura\"=f.\"clienteFactura\" and f1.\"fechaInicio\">='" + dateInicio(fechaInicio)
				+ "' and f1.\"fechaInicio\"<='" + dateFin(fechaFin) + "') as numCompro, sc.venta0, "
				+ "sc.venta12, sc.iva, sc.retiva as retIva, sc.retrenta as retRenta, 0.00 as total "
				+ "from negosys.facturas f inner join negosys.cliente cf on(f.\"clienteFactura\"=cf.clienteid) "
				+ "inner join negosys.persona p on(cf.personaid=p.personaid) inner join ( "
				+ "select f1.\"clienteFactura\", sum(f1.base0) as venta0, sum(f1.base12) as venta12, "
				+ "round(sum(f1.base12*" + redondearTotales(divide100(iva)) + "),2) as iva, sum(f1.retiva) as retiva, "
				+ "sum(f1.retrenta) as retrenta from negosys.facturas f1 where f1.\"fechaInicio\">='"
				+ dateInicio(fechaInicio) + "' and f1.\"fechaInicio\"<='" + dateFin(fechaFin)
				+ "' group by f1.\"clienteFactura\" ) sc on(sc.\"clienteFactura\"=f.\"clienteFactura\") "
				+ "where f.\"fechaInicio\">='" + dateInicio(fechaInicio) + "' and f.\"fechaInicio\"<='"
				+ dateFin(fechaFin) + "' order by p.cedula", VentasAnexo.class);
	}

	public List<VentasAnexo> obtenerParaAnexoNC(Date fechaInicio, Date fechaFin, BigDecimal iva) {
		return ventasAnexoDao.obtenerPorHql(
				"select distinct new VentasAnexo(p.cedula, '04', "
						+ "(select count(f1.clienteFactura) from NotaCredito nc1 inner join nc1.factura f1 "
						+ "where f1.clienteFactura=f.clienteFactura and nc1.fecha>=?2 and nc1.fecha<=?3), "
						+ "(round(coalesce((select sum(dnc.precioVenta*dnc.cantidad) from NotaCredito nc1 "
						+ "inner join nc1.detalleNotaCredito dnc inner join nc1.factura f1 "
						+ "inner join dnc.producto pr inner join pr.productosTarifas pt "
						+ "inner join pt.tarifa t where f1.clienteFactura=f.clienteFactura and nc1.fecha>=?2 "
						+ "and nc1.fecha<=?3 and t.id=1), cast(0.0 as big_decimal)),2)), "
						+ "(round(coalesce((select sum(dnc.precioVenta*dnc.cantidad) from NotaCredito nc1 "
						+ "inner join nc1.detalleNotaCredito dnc inner join nc1.factura f1 "
						+ "inner join dnc.producto pr inner join pr.productosTarifas pt "
						+ "inner join pt.tarifa t where f1.clienteFactura=f.clienteFactura and nc1.fecha>=?2 "
						+ "and nc1.fecha<=?3 and t.id=2), cast(0.0 as big_decimal)),2)), "
						+ "(round(coalesce((select sum(dnc.precioVenta*?1*dnc.cantidad) from NotaCredito nc1 "
						+ "inner join nc1.detalleNotaCredito dnc inner join nc1.factura f1 "
						+ "inner join dnc.producto pr inner join pr.productosTarifas pt "
						+ "inner join pt.tarifa t where f1.clienteFactura=f.clienteFactura and nc1.fecha>=?2 "
						+ "and nc1.fecha<=?3 and t.id=2), cast(0.0 as big_decimal)),2)), "
						+ "cast(0.00 as big_decimal), cast(0.00 as big_decimal)) from NotaCredito nc "
						+ "inner join nc.factura f inner join f.clienteFactura cf inner join cf.persona p "
						+ "where nc.fecha>=?2 and nc.fecha<=?3 order by p.cedula",
				new Object[] { redondearTotales(divide100(iva)), fechaInicio, dateCompleto(fechaFin) });
	}

	public List<VentasAnexo> obtenerParaAnexoVentasEst(Date fechaInicio, Date fechaFin) {
		return ventasAnexoDao.obtenerPorHql("select new VentasAnexo(l.codigoEstablecimiento, "
				+ "(round(coalesce((select sum(f.base0+f.base12) from Factura f "
				+ "where f.fechaInicio>=?1 and f.fechaInicio<=?2 and f.establecimiento=l.codigoEstablecimiento)-"
				+ "(select sum(dnc.precioVenta*dnc.cantidad) from NotaCredito nc "
				+ "inner join nc.detalleNotaCredito dnc where nc.fecha>=?1 and nc.fecha<=?2 "
				+ "and nc.establecimiento=l.codigoEstablecimiento), cast(0.0 as big_decimal)),2))) "
				+ "from Local l where l.activo=true", new Object[] { fechaInicio, dateCompleto(fechaFin) });
	}

}