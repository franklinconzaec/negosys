package ec.com.redepronik.negosys.invfac.dao;

import org.springframework.stereotype.Repository;

import ec.com.redepronik.negosys.invfac.entityAux.VentasAnexo;
import ec.com.redepronik.negosys.utils.dao.GenericDaoImpl;

@Repository
public class VentasAnexoDaoImpl extends GenericDaoImpl<VentasAnexo, Integer> implements VentasAnexoDao {

}