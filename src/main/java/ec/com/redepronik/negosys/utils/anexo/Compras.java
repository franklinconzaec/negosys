package ec.com.redepronik.negosys.utils.anexo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "detalleCompras" })
public class Compras {

	@XmlElement(required = true)
	protected List<DetalleCompras> detalleCompras;

	public List<DetalleCompras> getDetalleCompras() {
		if (this.detalleCompras == null) {
			this.detalleCompras = new ArrayList<DetalleCompras>();
		}
		return this.detalleCompras;
	}
}