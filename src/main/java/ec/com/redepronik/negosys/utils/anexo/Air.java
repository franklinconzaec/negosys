package ec.com.redepronik.negosys.utils.anexo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "detalleAir" })
public class Air {
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "codRetAir", "baseImpAir", "porcentajeAir", "valRetAir", "fechaPagoDiv",
			"imRentaSoc", "anioUtDiv" })
	public static class DetalleAir {
		@XmlElement(required = true)
		protected String codRetAir;
		@XmlElement(required = true)
		protected BigDecimal baseImpAir;
		@XmlElement(required = true)
		protected BigDecimal porcentajeAir;
		@XmlElement(required = true)
		protected BigDecimal valRetAir;
		protected String fechaPagoDiv;
		protected String imRentaSoc;
		protected String anioUtDiv;

		public String getCodRetAir() {
			return codRetAir;
		}

		public void setCodRetAir(String codRetAir) {
			this.codRetAir = codRetAir;
		}

		public BigDecimal getBaseImpAir() {
			return baseImpAir;
		}

		public void setBaseImpAir(BigDecimal baseImpAir) {
			this.baseImpAir = baseImpAir;
		}

		public BigDecimal getPorcentajeAir() {
			return porcentajeAir;
		}

		public void setPorcentajeAir(BigDecimal porcentajeAir) {
			this.porcentajeAir = porcentajeAir;
		}

		public BigDecimal getValRetAir() {
			return valRetAir;
		}

		public void setValRetAir(BigDecimal valRetAir) {
			this.valRetAir = valRetAir;
		}

		public String getFechaPagoDiv() {
			return fechaPagoDiv;
		}

		public void setFechaPagoDiv(String fechaPagoDiv) {
			this.fechaPagoDiv = fechaPagoDiv;
		}

		public String getImRentaSoc() {
			return imRentaSoc;
		}

		public void setImRentaSoc(String imRentaSoc) {
			this.imRentaSoc = imRentaSoc;
		}

		public String getAnioUtDiv() {
			return anioUtDiv;
		}

		public void setAnioUtDiv(String anioUtDiv) {
			this.anioUtDiv = anioUtDiv;
		}
	}

	@XmlElement(required = true)
	protected List<DetalleAir> detalleAir;

	public List<DetalleAir> getImpuesto() {
		if (this.detalleAir == null) {
			this.detalleAir = new ArrayList<DetalleAir>();
		}
		return this.detalleAir;
	}

}
