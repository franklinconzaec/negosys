package ec.com.redepronik.negosys.utils.anexo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "ventaEst" })
public class VentasEstablecimiento {

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "codEstab", "ventasEstab" })
	public static class VentaEst {
		@XmlElement(required = true)
		protected String codEstab;
		@XmlElement(required = true)
		protected BigDecimal ventasEstab;

		public String getCodEstab() {
			return codEstab;
		}

		public void setCodEstab(String codEstab) {
			this.codEstab = codEstab;
		}

		public BigDecimal getVentasEstab() {
			return ventasEstab;
		}

		public void setVentasEstab(BigDecimal ventasEstab) {
			this.ventasEstab = ventasEstab;
		}

	}

	@XmlElement(required = true)
	protected List<VentaEst> ventaEst;

	public List<VentaEst> getDetalleVentasEst() {
		if (this.ventaEst == null) {
			this.ventaEst = new ArrayList<VentaEst>();
		}
		return this.ventaEst;
	}
}