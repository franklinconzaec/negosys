package ec.com.redepronik.negosys.utils.anexo;

import static ec.com.redepronik.negosys.utils.UtilsDate.fechaFormatoString;
import static ec.com.redepronik.negosys.utils.UtilsMath.compareTo;
import static ec.com.redepronik.negosys.utils.UtilsMath.iva;
import static ec.com.redepronik.negosys.utils.UtilsMath.multiplicarDivide;
import static ec.com.redepronik.negosys.utils.UtilsMath.newBigDecimal;
import static ec.com.redepronik.negosys.utils.UtilsMath.parametro;
import static ec.com.redepronik.negosys.utils.UtilsMath.redondearTotales;
import static ec.com.redepronik.negosys.utils.documentosElectronicos.UtilsDocumentos.tipoIdentificacionComprador;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import ec.com.redepronik.negosys.invfac.entity.Anulado;
import ec.com.redepronik.negosys.invfac.entity.DetalleRetencion;
import ec.com.redepronik.negosys.invfac.entity.ImpuestoRetencion;
import ec.com.redepronik.negosys.invfac.entity.Retencion;
import ec.com.redepronik.negosys.invfac.entity.TarifaRetencion;
import ec.com.redepronik.negosys.invfac.entityAux.VentasAnexo;
import ec.com.redepronik.negosys.utils.anexo.Air.DetalleAir;
import ec.com.redepronik.negosys.utils.anexo.Anulados.DetalleAnulados;
import ec.com.redepronik.negosys.utils.anexo.DetalleCompras.PagoExterior;
import ec.com.redepronik.negosys.utils.anexo.Ventas.DetalleVentas;
import ec.com.redepronik.negosys.utils.anexo.VentasEstablecimiento.VentaEst;

public class UtilsAnexo {

	private static BigDecimal iva;

	private static Anexo armarAnexoXML(String anio, String mes, BigDecimal totalVentas, List<Retencion> listCompras,
			List<VentasAnexo> listVentasAnexos, List<VentasAnexo> listVentasEst, List<Anulado> listAnulados,
			int numEstablecimientos) {
		Anexo anexo = new Anexo();
		anexo.setTipoIDInformante("R");
		anexo.setIdInformante(parametro.getRuc());
		anexo.setRazonSocial(parametro.getRazonSocial().replaceAll("Ñ", "N"));
		anexo.setAnio(anio);
		anexo.setMes(mes);
		anexo.setNumEstabRuc(String.format("%03d", numEstablecimientos));
		anexo.setTotalVentas(parametro.getRuc().compareToIgnoreCase("0702011354001") == 0 ? new BigDecimal("0.00")
				: redondearTotales(totalVentas));
		anexo.setCodigoOperativo("IVA");
		anexo.setCompras(compras(listCompras));
		anexo.setVentas(ventas(listVentasAnexos));
		anexo.setVentasEstablecimiento(ventasEstablecimiento(listVentasEst));
		if (!listAnulados.isEmpty())
			anexo.setAnulados(anulados(listAnulados));
		return anexo;
	}

	private static Compras compras(List<Retencion> listCompras) {
		Compras compras = new Compras();
		for (Retencion r : listCompras)
			compras.getDetalleCompras().add(detalleCompras(r));
		return compras;
	}

	private static DetalleCompras detalleCompras(Retencion r) {
		DetalleCompras dc = new DetalleCompras();
		dc.setCodSustento(r.getSustentoTributario().getId());
		dc.setTpIdProv("01");
		dc.setIdProv(r.getProveedor().getPersona().getCedula());
		dc.setTipoComprobante(r.getTipoCompraRetencion().getId());
		dc.setParteRel("NO");
		dc.setFechaRegistro(fechaFormatoString(r.getFechaCompra()));
		dc.setEstablecimiento(r.getNumeroCompra().substring(0, 3));
		dc.setPuntoEmision(r.getNumeroCompra().substring(3, 6));
		dc.setSecuencial(r.getNumeroCompra().substring(6, 15));
		dc.setFechaEmision(fechaFormatoString(r.getFechaCompra()));
		dc.setAutorizacion(r.getAutorizacionCompra());
		dc.setBaseNoGraIva(new BigDecimal("0.00"));
		dc.setBaseImponible(r.getBase0());
		dc.setBaseImpGrav(r.getBase12());
		dc.setBaseImpExe(new BigDecimal("0.00"));
		dc.setMontoIce(new BigDecimal("0.00"));
		dc.setMontoIva(redondearTotales(iva(r.getBase12(), iva)));
		HashMap<String, BigDecimal> map = retencionesIVA(r.getDetallesRetenciones());
		dc.setValRetBien10(new BigDecimal("0.00"));
		dc.setValRetServ20(new BigDecimal("0.00"));
		dc.setValorRetBienes(map.get("bienes"));
		dc.setValRetServ50(new BigDecimal("0.00"));
		dc.setValorRetServicios(map.get("servicios"));
		dc.setValRetServ100(map.get("100"));
		dc.setTotbasesImpReemb(new BigDecimal("0.00"));
		dc.setPagoExterior(pagoExterior());
		if (compareTo(dc.getBaseImponible().add(dc.getBaseImpGrav()).add(dc.getMontoIva()), "1000.00") > 0)
			dc.setFormasDePago(formasPago("01"));
		dc.setAir(air(r.getDetallesRetenciones()));
		dc.setEstabRetencion1(r.getEstablecimiento());
		dc.setPtoEmiRetencion1(r.getPuntoEmision());
		dc.setSecRetencion1(r.getSecuencia());
		dc.setAutRetencion1(r.getAutorizacion());
		dc.setFechaEmiRet1(fechaFormatoString(r.getFechaEmision()));
		return dc;
	}

	private static HashMap<String, BigDecimal> retencionesIVA(List<DetalleRetencion> list) {
		HashMap<String, BigDecimal> map = new HashMap<String, BigDecimal>();
		map.put("bienes", redondearTotales(newBigDecimal()));
		map.put("servicios", redondearTotales(newBigDecimal()));
		map.put("100", redondearTotales(newBigDecimal()));
		for (DetalleRetencion dr : list)
			if (dr.getImpuestoRetencion() == ImpuestoRetencion.IV) {
				BigDecimal valor = redondearTotales(
						multiplicarDivide(dr.getBaseImponible(), dr.getPorcentajeRetencion()));
				if (dr.getTarifa() == TarifaRetencion.I3)
					map.put("bienes", valor);
				else if (dr.getTarifa() == TarifaRetencion.I7)
					map.put("servicios", valor);
				else if (dr.getTarifa() == TarifaRetencion.I1)
					map.put("100", valor);
			}
		return map;
	}

	private static PagoExterior pagoExterior() {
		PagoExterior pe = new PagoExterior();
		pe.setPagoLocExt("01");
		pe.setPaisEfecPago("NA");
		pe.setAplicConvDobTrib("NA");
		pe.setPagExtSujRetNorLeg("NA");
		return pe;
	}

	private static FormasDePago formasPago(String codigo) {
		FormasDePago fdp = new FormasDePago();
		fdp.setFormaPago(codigo);
		return fdp;
	}

	private static Air air(List<DetalleRetencion> list) {
		Air air = new Air();
		for (DetalleRetencion dr : list)
			if (dr.getImpuestoRetencion() == ImpuestoRetencion.RE)
				air.getImpuesto().add(detalleAir(dr));
		return air;
	}

	private static DetalleAir detalleAir(DetalleRetencion dr) {
		DetalleAir da = new DetalleAir();
		da.setCodRetAir(dr.getTarifa().getId());
		da.setBaseImpAir(dr.getBaseImponible());
		da.setPorcentajeAir(dr.getPorcentajeRetencion());
		da.setValRetAir(redondearTotales(multiplicarDivide(dr.getBaseImponible(), dr.getPorcentajeRetencion())));
		return da;
	}

	private static Ventas ventas(List<VentasAnexo> listVentasAnexos) {
		Ventas ventas = new Ventas();
		for (VentasAnexo va : listVentasAnexos)
			ventas.getDetalleVentas().add(detalleVentas(va));
		return ventas;
	}

	private static DetalleVentas detalleVentas(VentasAnexo va) {
		DetalleVentas dv = new DetalleVentas();
		dv.setTpIdCliente(tipoIdentificacionComprador(va.getCedula()));
		dv.setIdCliente(va.getCedula());
		if (dv.getTpIdCliente().compareTo("07") != 0)
			dv.setParteRelVtas("NO");
		dv.setTipoComprobante(va.getTipoComprobante());
		dv.setTipoEmision(parametro.getRuc().compareToIgnoreCase("0702011354001") == 0 ? "E" : "F");
		dv.setNumeroComprobantes((int) va.getNumCompro());
		dv.setBaseNoGraIva(new BigDecimal("0.00"));
		dv.setBaseImponible(va.getVenta0());
		dv.setBaseImpGrav(va.getVenta12());
		dv.setMontoIva(va.getIva());
		dv.setMontoIce(new BigDecimal("0.00"));
		dv.setValorRetIva(va.getRetIva());
		dv.setValorRetRenta(va.getRetRenta());
		if (va.getTipoComprobante().compareTo("04") != 0)
			dv.setFormasDePago(formasPago("01"));
		return dv;
	}

	private static VentasEstablecimiento ventasEstablecimiento(List<VentasAnexo> listVentasEst) {
		VentasEstablecimiento ventasEstablecimiento = new VentasEstablecimiento();
		for (VentasAnexo va : listVentasEst)
			ventasEstablecimiento.getDetalleVentasEst().add(detalleVentasEst(va));
		return ventasEstablecimiento;
	}

	private static VentaEst detalleVentasEst(VentasAnexo va) {
		VentaEst ve = new VentaEst();
		ve.setCodEstab(va.getCedula());
		ve.setVentasEstab(
				parametro.getRuc().compareToIgnoreCase("0702011354001") == 0 ? new BigDecimal("0.00") : va.getTotal());
		return ve;
	}

	private static Anulados anulados(List<Anulado> listAnulados) {
		Anulados anulados = new Anulados();
		for (Anulado a : listAnulados)
			anulados.getDetalleAnulados().add(detalleAnulados(a));
		return anulados;
	}

	private static DetalleAnulados detalleAnulados(Anulado a) {
		DetalleAnulados da = new DetalleAnulados();
		da.setTipoComprobante(a.getTipoComprobante().getId());
		da.setEstablecimiento(a.getEstablecimiento());
		da.setPuntoEmision(a.getPuntoEmision());
		da.setSecuencialInicio(a.getSecuenciaInicio());
		da.setSecuencialFin(a.getSecuenciaFin());
		da.setAutorizacion(a.getAutorizacion());
		return da;
	}

	public static Anexo anexoXML(String anio, String mes, BigDecimal totalVentas, List<Retencion> listCompras,
			List<VentasAnexo> listVentasAnexos, List<VentasAnexo> listVentasEst, List<Anulado> listAnulados,
			int numEstablecimientos, BigDecimal iva) {
		UtilsAnexo.iva = iva;
		return armarAnexoXML(anio, mes, totalVentas, listCompras, listVentasAnexos, listVentasEst, listAnulados,
				numEstablecimientos);
	}

}
