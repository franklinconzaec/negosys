package ec.com.redepronik.negosys.utils.anexo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "detalleAnulados" })
public class Anulados {

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "tipoComprobante", "establecimiento", "puntoEmision", "secuencialInicio",
			"secuencialFin", "autorizacion" })
	public static class DetalleAnulados {
		@XmlElement(required = true)
		protected String tipoComprobante;
		@XmlElement(required = true)
		protected String establecimiento;
		@XmlElement(required = true)
		protected String puntoEmision;
		@XmlElement(required = true)
		protected String secuencialInicio;
		@XmlElement(required = true)
		protected String secuencialFin;
		@XmlElement(required = true)
		protected String autorizacion;

		public String getTipoComprobante() {
			return tipoComprobante;
		}

		public void setTipoComprobante(String tipoComprobante) {
			this.tipoComprobante = tipoComprobante;
		}

		public String getEstablecimiento() {
			return establecimiento;
		}

		public void setEstablecimiento(String establecimiento) {
			this.establecimiento = establecimiento;
		}

		public String getPuntoEmision() {
			return puntoEmision;
		}

		public void setPuntoEmision(String puntoEmision) {
			this.puntoEmision = puntoEmision;
		}

		public String getSecuencialInicio() {
			return secuencialInicio;
		}

		public void setSecuencialInicio(String secuencialInicio) {
			this.secuencialInicio = secuencialInicio;
		}

		public String getSecuencialFin() {
			return secuencialFin;
		}

		public void setSecuencialFin(String secuencialFin) {
			this.secuencialFin = secuencialFin;
		}

		public String getAutorizacion() {
			return autorizacion;
		}

		public void setAutorizacion(String autorizacion) {
			this.autorizacion = autorizacion;
		}

	}

	@XmlElement(required = true)
	protected List<DetalleAnulados> detalleAnulados;

	public List<DetalleAnulados> getDetalleAnulados() {
		if (this.detalleAnulados == null) {
			this.detalleAnulados = new ArrayList<DetalleAnulados>();
		}
		return this.detalleAnulados;
	}
}