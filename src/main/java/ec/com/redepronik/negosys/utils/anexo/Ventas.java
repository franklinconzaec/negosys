package ec.com.redepronik.negosys.utils.anexo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "detalleVentas" })
public class Ventas {

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "tpIdCliente", "idCliente", "parteRelVtas", "tipoComprobante", "tipoEmision",
			"numeroComprobantes", "baseNoGraIva", "baseImponible", "baseImpGrav", "montoIva", "montoIce", "valorRetIva",
			"valorRetRenta", "formasDePago" })
	public static class DetalleVentas {
		@XmlElement(required = true)
		protected String tpIdCliente;
		@XmlElement(required = true)
		protected String idCliente;
		@XmlElement(defaultValue = "NO", required = true)
		protected String parteRelVtas;
		@XmlElement(required = true)
		protected String tipoComprobante;
		@XmlElement(defaultValue = "E", required = true)
		protected String tipoEmision;
		@XmlElement(required = true)
		protected int numeroComprobantes;
		@XmlElement(required = true)
		protected BigDecimal baseNoGraIva;
		@XmlElement(required = true)
		protected BigDecimal baseImponible;
		@XmlElement(required = true)
		protected BigDecimal baseImpGrav;
		@XmlElement(required = true)
		protected BigDecimal montoIva;
		@XmlElement(defaultValue = "0.00", required = true)
		protected BigDecimal montoIce;
		@XmlElement(required = true)
		protected BigDecimal valorRetIva;
		@XmlElement(required = true)
		protected BigDecimal valorRetRenta;
		@XmlElement(required = true)
		protected FormasDePago formasDePago;

		public String getTpIdCliente() {
			return tpIdCliente;
		}

		public void setTpIdCliente(String tpIdCliente) {
			this.tpIdCliente = tpIdCliente;
		}

		public String getIdCliente() {
			return idCliente;
		}

		public void setIdCliente(String idCliente) {
			this.idCliente = idCliente;
		}

		public String getParteRelVtas() {
			return parteRelVtas;
		}

		public void setParteRelVtas(String parteRelVtas) {
			this.parteRelVtas = parteRelVtas;
		}

		public String getTipoComprobante() {
			return tipoComprobante;
		}

		public void setTipoComprobante(String tipoComprobante) {
			this.tipoComprobante = tipoComprobante;
		}

		public String getTipoEmision() {
			return tipoEmision;
		}

		public void setTipoEmision(String tipoEmision) {
			this.tipoEmision = tipoEmision;
		}

		public int getNumeroComprobantes() {
			return numeroComprobantes;
		}

		public void setNumeroComprobantes(int numeroComprobantes) {
			this.numeroComprobantes = numeroComprobantes;
		}

		public BigDecimal getBaseNoGraIva() {
			return baseNoGraIva;
		}

		public void setBaseNoGraIva(BigDecimal baseNoGraIva) {
			this.baseNoGraIva = baseNoGraIva;
		}

		public BigDecimal getBaseImponible() {
			return baseImponible;
		}

		public void setBaseImponible(BigDecimal baseImponible) {
			this.baseImponible = baseImponible;
		}

		public BigDecimal getBaseImpGrav() {
			return baseImpGrav;
		}

		public void setBaseImpGrav(BigDecimal baseImpGrav) {
			this.baseImpGrav = baseImpGrav;
		}

		public BigDecimal getMontoIva() {
			return montoIva;
		}

		public void setMontoIva(BigDecimal montoIva) {
			this.montoIva = montoIva;
		}

		public BigDecimal getMontoIce() {
			return montoIce;
		}

		public void setMontoIce(BigDecimal montoIce) {
			this.montoIce = montoIce;
		}

		public BigDecimal getValorRetIva() {
			return valorRetIva;
		}

		public void setValorRetIva(BigDecimal valorRetIva) {
			this.valorRetIva = valorRetIva;
		}

		public BigDecimal getValorRetRenta() {
			return valorRetRenta;
		}

		public void setValorRetRenta(BigDecimal valorRetRenta) {
			this.valorRetRenta = valorRetRenta;
		}

		public FormasDePago getFormasDePago() {
			return formasDePago;
		}

		public void setFormasDePago(FormasDePago formasDePago) {
			this.formasDePago = formasDePago;
		}

	}

	@XmlElement(required = true)
	protected List<DetalleVentas> detalleVentas;

	public List<DetalleVentas> getDetalleVentas() {
		if (this.detalleVentas == null) {
			this.detalleVentas = new ArrayList<DetalleVentas>();
		}
		return this.detalleVentas;
	}
}