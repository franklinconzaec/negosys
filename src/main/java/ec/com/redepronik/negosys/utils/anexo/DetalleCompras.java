package ec.com.redepronik.negosys.utils.anexo;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "codSustento", "tpIdProv", "idProv", "tipoComprobante", "parteRel", "fechaRegistro",
		"establecimiento", "puntoEmision", "secuencial", "fechaEmision", "autorizacion", "baseNoGraIva",
		"baseImponible", "baseImpGrav", "baseImpExe", "montoIce", "montoIva", "valRetBien10", "valRetServ20",
		"valorRetBienes", "valRetServ50", "valorRetServicios", "valRetServ100", "totbasesImpReemb", "pagoExterior",
		"formasDePago", "air", "estabRetencion1", "ptoEmiRetencion1", "secRetencion1", "autRetencion1", "fechaEmiRet1",
		"docModificado", "estabModificado", "ptoEmiModificado", "secModificado", "autModificado" })
public class DetalleCompras {
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "pagoLocExt", "paisEfecPago", "aplicConvDobTrib", "pagExtSujRetNorLeg" })
	public static class PagoExterior {
		@XmlElement(defaultValue = "01", required = true)
		protected String pagoLocExt;
		@XmlElement(defaultValue = "NA")
		protected String paisEfecPago;
		@XmlElement(defaultValue = "NA")
		protected String aplicConvDobTrib;
		@XmlElement(defaultValue = "NA")
		protected String pagExtSujRetNorLeg;

		public String getPagoLocExt() {
			return pagoLocExt;
		}

		public void setPagoLocExt(String pagoLocExt) {
			this.pagoLocExt = pagoLocExt;
		}

		public String getPaisEfecPago() {
			return paisEfecPago;
		}

		public void setPaisEfecPago(String paisEfecPago) {
			this.paisEfecPago = paisEfecPago;
		}

		public String getAplicConvDobTrib() {
			return aplicConvDobTrib;
		}

		public void setAplicConvDobTrib(String aplicConvDobTrib) {
			this.aplicConvDobTrib = aplicConvDobTrib;
		}

		public String getPagExtSujRetNorLeg() {
			return pagExtSujRetNorLeg;
		}

		public void setPagExtSujRetNorLeg(String pagExtSujRetNorLeg) {
			this.pagExtSujRetNorLeg = pagExtSujRetNorLeg;
		}

	}

	@XmlElement(required = true)
	protected String codSustento;
	@XmlElement(required = true)
	protected String tpIdProv;
	@XmlElement(required = true)
	protected String idProv;
	@XmlElement(required = true)
	protected String tipoComprobante;
	@XmlElement(defaultValue = "NO", required = true)
	protected String parteRel;
	@XmlElement(required = true)
	protected String fechaRegistro;
	@XmlElement(required = true)
	protected String establecimiento;
	@XmlElement(required = true)
	protected String puntoEmision;
	@XmlElement(required = true)
	protected String secuencial;
	@XmlElement(required = true)
	protected String fechaEmision;
	@XmlElement(required = true)
	protected String autorizacion;
	@XmlElement(required = true)
	protected BigDecimal baseNoGraIva;
	@XmlElement(required = true)
	protected BigDecimal baseImponible;
	@XmlElement(required = true)
	protected BigDecimal baseImpGrav;
	@XmlElement(defaultValue = "0.00", required = true)
	protected BigDecimal baseImpExe;
	@XmlElement(required = true)
	protected BigDecimal montoIce;
	@XmlElement(required = true)
	protected BigDecimal montoIva;
	@XmlElement(defaultValue = "0.00", required = true)
	protected BigDecimal valRetBien10;
	@XmlElement(defaultValue = "0.00", required = true)
	protected BigDecimal valRetServ20;
	@XmlElement(defaultValue = "0.00", required = true)
	protected BigDecimal valorRetBienes;
	@XmlElement(defaultValue = "0.00", required = true)
	protected BigDecimal valRetServ50;
	@XmlElement(defaultValue = "0.00", required = true)
	protected BigDecimal valorRetServicios;
	@XmlElement(defaultValue = "0.00", required = true)
	protected BigDecimal valRetServ100;
	@XmlElement(defaultValue = "0.00", required = true)
	protected BigDecimal totbasesImpReemb;
	@XmlElement(required = true)
	protected PagoExterior pagoExterior;
	@XmlElement(required = true)
	protected FormasDePago formasDePago;
	protected Air air;
	protected String estabRetencion1;
	protected String ptoEmiRetencion1;
	protected String secRetencion1;
	protected String autRetencion1;
	protected String fechaEmiRet1;
	protected String docModificado;
	protected String estabModificado;
	protected String ptoEmiModificado;
	protected String secModificado;
	protected String autModificado;

	public String getCodSustento() {
		return codSustento;
	}

	public void setCodSustento(String codSustento) {
		this.codSustento = codSustento;
	}

	public String getTpIdProv() {
		return tpIdProv;
	}

	public void setTpIdProv(String tpIdProv) {
		this.tpIdProv = tpIdProv;
	}

	public String getIdProv() {
		return idProv;
	}

	public void setIdProv(String idProv) {
		this.idProv = idProv;
	}

	public String getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(String tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	public String getParteRel() {
		return parteRel;
	}

	public void setParteRel(String parteRel) {
		this.parteRel = parteRel;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getEstablecimiento() {
		return establecimiento;
	}

	public void setEstablecimiento(String establecimiento) {
		this.establecimiento = establecimiento;
	}

	public String getPuntoEmision() {
		return puntoEmision;
	}

	public void setPuntoEmision(String puntoEmision) {
		this.puntoEmision = puntoEmision;
	}

	public String getSecuencial() {
		return secuencial;
	}

	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}

	public String getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public String getAutorizacion() {
		return autorizacion;
	}

	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}

	public BigDecimal getBaseNoGraIva() {
		return baseNoGraIva;
	}

	public void setBaseNoGraIva(BigDecimal baseNoGraIva) {
		this.baseNoGraIva = baseNoGraIva;
	}

	public BigDecimal getBaseImponible() {
		return baseImponible;
	}

	public void setBaseImponible(BigDecimal baseImponible) {
		this.baseImponible = baseImponible;
	}

	public BigDecimal getBaseImpGrav() {
		return baseImpGrav;
	}

	public void setBaseImpGrav(BigDecimal baseImpGrav) {
		this.baseImpGrav = baseImpGrav;
	}

	public BigDecimal getBaseImpExe() {
		return baseImpExe;
	}

	public void setBaseImpExe(BigDecimal baseImpExe) {
		this.baseImpExe = baseImpExe;
	}

	public BigDecimal getMontoIce() {
		return montoIce;
	}

	public void setMontoIce(BigDecimal montoIce) {
		this.montoIce = montoIce;
	}

	public BigDecimal getMontoIva() {
		return montoIva;
	}

	public void setMontoIva(BigDecimal montoIva) {
		this.montoIva = montoIva;
	}

	public BigDecimal getValRetBien10() {
		return valRetBien10;
	}

	public void setValRetBien10(BigDecimal valRetBien10) {
		this.valRetBien10 = valRetBien10;
	}

	public BigDecimal getValRetServ20() {
		return valRetServ20;
	}

	public void setValRetServ20(BigDecimal valRetServ20) {
		this.valRetServ20 = valRetServ20;
	}

	public BigDecimal getValorRetBienes() {
		return valorRetBienes;
	}

	public void setValorRetBienes(BigDecimal valorRetBienes) {
		this.valorRetBienes = valorRetBienes;
	}

	public BigDecimal getValRetServ50() {
		return valRetServ50;
	}

	public void setValRetServ50(BigDecimal valRetServ50) {
		this.valRetServ50 = valRetServ50;
	}

	public BigDecimal getValorRetServicios() {
		return valorRetServicios;
	}

	public void setValorRetServicios(BigDecimal valorRetServicios) {
		this.valorRetServicios = valorRetServicios;
	}

	public BigDecimal getValRetServ100() {
		return valRetServ100;
	}

	public void setValRetServ100(BigDecimal valRetServ100) {
		this.valRetServ100 = valRetServ100;
	}

	public BigDecimal getTotbasesImpReemb() {
		return totbasesImpReemb;
	}

	public void setTotbasesImpReemb(BigDecimal totbasesImpReemb) {
		this.totbasesImpReemb = totbasesImpReemb;
	}

	public PagoExterior getPagoExterior() {
		return pagoExterior;
	}

	public void setPagoExterior(PagoExterior pagoExterior) {
		this.pagoExterior = pagoExterior;
	}

	public FormasDePago getFormasDePago() {
		return formasDePago;
	}

	public void setFormasDePago(FormasDePago formasDePago) {
		this.formasDePago = formasDePago;
	}

	public Air getAir() {
		return air;
	}

	public void setAir(Air air) {
		this.air = air;
	}

	public String getEstabRetencion1() {
		return estabRetencion1;
	}

	public void setEstabRetencion1(String estabRetencion1) {
		this.estabRetencion1 = estabRetencion1;
	}

	public String getPtoEmiRetencion1() {
		return ptoEmiRetencion1;
	}

	public void setPtoEmiRetencion1(String ptoEmiRetencion1) {
		this.ptoEmiRetencion1 = ptoEmiRetencion1;
	}

	public String getSecRetencion1() {
		return secRetencion1;
	}

	public void setSecRetencion1(String secRetencion1) {
		this.secRetencion1 = secRetencion1;
	}

	public String getAutRetencion1() {
		return autRetencion1;
	}

	public void setAutRetencion1(String autRetencion1) {
		this.autRetencion1 = autRetencion1;
	}

	public String getFechaEmiRet1() {
		return fechaEmiRet1;
	}

	public void setFechaEmiRet1(String fechaEmiRet1) {
		this.fechaEmiRet1 = fechaEmiRet1;
	}

	public String getDocModificado() {
		return docModificado;
	}

	public void setDocModificado(String docModificado) {
		this.docModificado = docModificado;
	}

	public String getEstabModificado() {
		return estabModificado;
	}

	public void setEstabModificado(String estabModificado) {
		this.estabModificado = estabModificado;
	}

	public String getPtoEmiModificado() {
		return ptoEmiModificado;
	}

	public void setPtoEmiModificado(String ptoEmiModificado) {
		this.ptoEmiModificado = ptoEmiModificado;
	}

	public String getSecModificado() {
		return secModificado;
	}

	public void setSecModificado(String secModificado) {
		this.secModificado = secModificado;
	}

	public String getAutModificado() {
		return autModificado;
	}

	public void setAutModificado(String autModificado) {
		this.autModificado = autModificado;
	}

}
