package ec.com.redepronik.negosys.utils.anexo;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "TipoIDInformante", "IdInformante", "razonSocial", "Anio", "Mes", "numEstabRuc",
		"totalVentas", "codigoOperativo", "compras", "ventas", "ventasEstablecimiento", "anulados" })
@XmlRootElement(name = "iva")
public class Anexo {
	@XmlElement(defaultValue = "R", required = true)
	protected String TipoIDInformante;
	@XmlElement(required = true)
	protected String IdInformante;
	@XmlElement(required = true)
	protected String razonSocial;
	@XmlElement(required = true)
	protected String Anio;
	@XmlElement(required = true)
	protected String Mes;
	@XmlElement(required = true)
	protected String numEstabRuc;
	@XmlElement(required = true)
	protected BigDecimal totalVentas;
	@XmlElement(defaultValue = "IVA", required = true)
	protected String codigoOperativo;
	@XmlElement(required = true)
	protected Compras compras;
	@XmlElement(required = true)
	protected Ventas ventas;
	@XmlElement(required = true)
	protected VentasEstablecimiento ventasEstablecimiento;
	@XmlElement(required = true)
	protected Anulados anulados;

	public String getTipoIDInformante() {
		return TipoIDInformante;
	}

	public void setTipoIDInformante(String tipoIDInformante) {
		TipoIDInformante = tipoIDInformante;
	}

	public String getIdInformante() {
		return IdInformante;
	}

	public void setIdInformante(String idInformante) {
		IdInformante = idInformante;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getAnio() {
		return Anio;
	}

	public void setAnio(String anio) {
		Anio = anio;
	}

	public String getMes() {
		return Mes;
	}

	public void setMes(String mes) {
		Mes = mes;
	}

	public String getNumEstabRuc() {
		return numEstabRuc;
	}

	public void setNumEstabRuc(String numEstabRuc) {
		this.numEstabRuc = numEstabRuc;
	}

	public BigDecimal getTotalVentas() {
		return totalVentas;
	}

	public void setTotalVentas(BigDecimal totalVentas) {
		this.totalVentas = totalVentas;
	}

	public String getCodigoOperativo() {
		return codigoOperativo;
	}

	public void setCodigoOperativo(String codigoOperativo) {
		this.codigoOperativo = codigoOperativo;
	}

	public Compras getCompras() {
		return compras;
	}

	public void setCompras(Compras compras) {
		this.compras = compras;
	}

	public Ventas getVentas() {
		return ventas;
	}

	public void setVentas(Ventas ventas) {
		this.ventas = ventas;
	}

	public VentasEstablecimiento getVentasEstablecimiento() {
		return ventasEstablecimiento;
	}

	public void setVentasEstablecimiento(VentasEstablecimiento ventasEstablecimiento) {
		this.ventasEstablecimiento = ventasEstablecimiento;
	}

	public Anulados getAnulados() {
		return anulados;
	}

	public void setAnulados(Anulados anulados) {
		this.anulados = anulados;
	}

}
