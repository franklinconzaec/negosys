package ec.com.redepronik.negosys.m;

import static ec.com.redepronik.negosys.utils.Utils.expRegular;
import static ec.com.redepronik.negosys.utils.UtilsAplicacion.execute;
import static ec.com.redepronik.negosys.utils.UtilsAplicacion.presentaMensaje;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import ec.com.redepronik.negosys.invfac.entity.EstadoProductoVenta;
import ec.com.redepronik.negosys.invfac.entity.Local;
import ec.com.redepronik.negosys.invfac.entity.Pedido;
import ec.com.redepronik.negosys.invfac.entity.Producto;
import ec.com.redepronik.negosys.invfac.entity.TipoPrecioProducto;
import ec.com.redepronik.negosys.invfac.entityAux.FacturaReporte;
import ec.com.redepronik.negosys.invfac.service.LocalService;
import ec.com.redepronik.negosys.invfac.service.PedidoService;
import ec.com.redepronik.negosys.invfac.service.ProductoService;
import ec.com.redepronik.negosys.rrhh.entity.Cliente;
import ec.com.redepronik.negosys.rrhh.entity.EmpleadoCargo;
import ec.com.redepronik.negosys.rrhh.entity.Persona;
import ec.com.redepronik.negosys.rrhh.service.ClienteService;
import ec.com.redepronik.negosys.rrhh.service.EmpleadoService;

@Controller
@Scope("session")
public class NuevoPedidoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private PedidoService pedidoService;

	@Autowired
	private ProductoService productoService;

	@Autowired
	private ClienteService clienteService;

	@Autowired
	private LocalService localService;

	@Autowired
	private EmpleadoService empleadoService;

	private Pedido pedido;

	private List<FacturaReporte> listaFacturaReporte;

	private List<Local> listaLocales;
	private Local local;
	private EmpleadoCargo vendedor;

	private Producto producto;
	private String criterioBusquedaRapida;
	private List<Producto> listaBusquedaRapida;

	private String cliente;
	private String JSON;

	public NuevoPedidoBean() {

	}

	@PostConstruct
	public void init() {
		String cedula = SecurityContextHolder.getContext().getAuthentication().getName();
		vendedor = empleadoService.obtenerEmpleadoCargoPorCedulaAndCargo(cedula, 5);
		listaLocales = localService.obtenerPorCedulaAndCargo(cedula, 4);
		local = new Local();
		if (listaLocales.size() == 1) {
			local = listaLocales.get(0);
		}
		limpiar();
	}

	public void limpiar() {
		pedido = new Pedido();
		pedido.setCliente(new Cliente());
		pedido.setVendedor(vendedor);
		pedido.setEstablecimiento(local.getCodigoEstablecimiento());

		cliente = "";
		JSON = "";

		listaFacturaReporte = new ArrayList<FacturaReporte>();
	}

	public void busquedaRapida() {
		listaBusquedaRapida = productoService.obtenerPorLocal(criterioBusquedaRapida, null, local.getId());
	}

	public void insertarProductoRapido(SelectEvent event) {
		FacturaReporte fr = new FacturaReporte();
		listaFacturaReporte.add(fr);
		Producto producto = (Producto) event.getObject();
		fr.setProducto(producto);
		pedidoService.cargarProductoLista(fr);
		presentaMensaje(FacesMessage.SEVERITY_INFO, "AGREGÓ PRODUCTO " + producto.getNombre());
		if (!JSON.contains(fr.getCodigo())) {
			String tipoPrecios = "";
			for (TipoPrecioProducto tpp : producto.getTipoPrecioProductos())
				tipoPrecios += "{\"id\" : " + tpp.getId() + ", \"nombre\":\"" + tpp.getNombre() + "\", \"valor\":"
						+ tpp.getValor() + ", \"porcentajePrecioFijo\":" + tpp.getPorcentajePrecioFijo() + "}, ";
			JSON += "{\"codigo\" : \"" + fr.getCodigo() + "\", \"impuesto\":\"" + fr.getImpuesto() + "\", \"precio\":"
					+ producto.getPrecio() + ", \"tipoPrecios\":[" + tipoPrecios + "]}, ";
		}
	}

	public void cambiarTipoPrecio(FacturaReporte facturaReporte) {
		facturaReporte.setPrecioUnitVenta(
				productoService.calcularPrecio(facturaReporte.getProducto(), facturaReporte.getPrecioId()));
	}

	public boolean comprobarLocal() {
		if (pedido.getEstablecimiento() == null || pedido.getEstablecimiento().compareToIgnoreCase("") == 0) {
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "DEBE ESCOJER UN LOCAL");
			return false;
		}
		return true;
	}

	public boolean comprobarTodo() {
		if (pedido.getCliente().getPersona() == null) {
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "INGRESE UN CLIENTE");
			return true;
		} else if (pedido.getEstablecimiento() == null || pedido.getEstablecimiento().compareToIgnoreCase("") == 0) {
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "ESCOJA UN LOCAL");
			return true;
		} else if (pedido.getVendedor().getId() == 0) {
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "INGRESE UN VENDEDOR");
			return true;
		} else if (pedidoService.convertirListaFacturaReporteListaPedidoDetalle(listaFacturaReporte, pedido))
			return true;

		return false;
	}

	public void insertar(ActionEvent actionEvent) {
		if (!comprobarTodo())
			if (pedidoService.insertar(pedido).getId() != 0)
				limpiar();
	}

	public void obtenerCliente() {
		if (expRegular(cliente, "[0-9]+")) {
			int codigoCliente = Integer.parseInt(cliente);
			List<Persona> lista = clienteService.obtener(null, codigoCliente);
			if (lista == null || lista.isEmpty())
				presentaMensaje(FacesMessage.SEVERITY_ERROR, "NO EXISTE ESE CODIGO DE CLIENTE");
			else {
				Persona p = lista.get(0);
				pedido.setCliente(p.getCliente());
				cliente = p.getCliente().getId().toString().concat("-").concat(p.getCedula()).concat("-")
						.concat(p.getApellido()).concat(" ").concat(p.getNombre());
			}
		} else
			presentaMensaje(FacesMessage.SEVERITY_ERROR, "INGRESE SOLO EL CODIGO DEL CLIENTE");
	}

	public List<String> obtenerProductosPorBusqueda(String criterioProductoBusqueda) {
		if (comprobarLocal())
			return productoService.obtenerListaStringPorLocal(criterioProductoBusqueda, local.getId());
		return new ArrayList<String>();
	}

	public void guardarDetallePedido() {
		execute("guardarProductos([" + JSON + "])");
	}

	// GETTERS Y SETTERS

	public String getCliente() {
		return cliente;
	}

	public String getCriterioBusquedaRapida() {
		return criterioBusquedaRapida;
	}

	public List<Producto> getListaBusquedaRapida() {
		return listaBusquedaRapida;
	}

	public EstadoProductoVenta[] getListaEstadoProductoVenta() {
		return EstadoProductoVenta.values();
	}

	public List<FacturaReporte> getListaFacturaReporte() {
		return listaFacturaReporte;
	}

	public List<Local> getListaLocales() {

		return listaLocales;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public void setCriterioBusquedaRapida(String criterioBusquedaRapida) {
		this.criterioBusquedaRapida = criterioBusquedaRapida;
	}

	public void setListaBusquedaRapida(List<Producto> listaBusquedaRapida) {
		this.listaBusquedaRapida = listaBusquedaRapida;
	}

	public void setListaFacturaReporte(List<FacturaReporte> listaFacturaReporte) {
		this.listaFacturaReporte = listaFacturaReporte;
	}

	public void setListaLocales(List<Local> listaLocales) {
		this.listaLocales = listaLocales;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public String getJSON() {
		return JSON;
	}

	public void setJSON(String jSON) {
		JSON = jSON;
	}

}